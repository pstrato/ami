using System;
using System.IO;
using System.Threading.Tasks;
using Ami.Core.Collections;
using Ami.Store.Events;
using Ami.Store.Resources;
using Ami.Synchronize;

namespace Ami.Store.Synchronise
{

    /// <summary> Synchronized storage. </summary>
    public class SynchronizedStorage : SynchronizedObject, IStorage
    {
        class SynchronizedStream : Stream {
            Stream stream;

            Action onDispose;

            public SynchronizedStream(Stream stream, Action onDispose) {
                this.stream = stream;
                this.onDispose = onDispose;
            }

            public override bool CanRead => stream.CanRead;

            public override bool CanSeek => stream.CanSeek;

            public override bool CanWrite => stream.CanWrite;

            public override long Length => stream.Length;

            public override long Position { get => stream.Position; set => stream.Position = value; }

            public override void Flush() => stream.Flush();

            public override int Read(byte[] buffer, int offset, int count) => stream.Read(buffer, offset, count);

            public override long Seek(long offset, SeekOrigin origin) => stream.Seek(offset, origin);

            public override void SetLength(long value) => stream.SetLength(value);

            public override void Write(byte[] buffer, int offset, int count) => stream.Write(buffer, offset, count);

            protected override void Dispose(bool disposing) {
                base.Dispose(disposing);
                onDispose();
                onDispose = null;
                stream = null;
            }
        }
        
        IStorage storage;

        bool disposeStorage;

        public SynchronizedStorage(IStorage storage, bool disposeStorage = true)
        {
            this.storage = storage ?? throw new ArgumentNullException(nameof(storage));
            this.disposeStorage = disposeStorage;
        }

        public async Task<Stream> Content(Path<Store.Resources.File> path)
        {
            await OpenReadLock();
            return new SynchronizedStream(await storage.Content(path), () => CloseReadLock());
        }

        public Task<Store.Resources.File> Get(Path<Store.Resources.File> path)
        {
            return WithReadLock(() => storage.Get(path));
        }

        public Task<Folder> Get(Path<Folder> path)
        {
            return WithReadLock(() => storage.Get(path));
        }

        public Task<ISlicedEnumerable<Store.Resources.File>> ListFiles(Path<Folder> basePath = default(Path<Folder>))
        {
            return WithReadLock(() => storage.ListFiles(basePath));
        }

        public Task<ISlicedEnumerable<Folder>> ListFolders(Path<Folder> basePath = default(Path<Folder>))
        {
            return WithReadLock(() => storage.ListFolders(basePath));
        }

        public Task<IDisposable> OnChange(Action action)
        {
            return storage.OnChange(action);
        }

        public Task<EventId> Update(EventId eventId = default(EventId))
        {
            return WithWriteLock(() => storage.Update(eventId));
        }

        protected override async Task DisposeManagedResources()
        {
            await base.DisposeManagedResources();
            if (disposeStorage) storage.Dispose();
            storage = null;
        }
    }
}