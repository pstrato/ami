using System;
using System.Threading.Tasks;
using Ami.Core.Collections;
using Ami.Store.Events;
using Ami.Store.Events.Transactions;
using Ami.Synchronize;

namespace Ami.Store.Synchronise
{

    /// <summary> Synchronized event stream. </summary>
    public class SynchronizedEventStream : SynchronizedObject, IEventStream
    {
        IEventStream eventStream;

        bool disposeEventStream;

        public SynchronizedEventStream(IEventStream eventStream, bool disposeEventStream = true)
        {
            this.eventStream = eventStream ?? throw new ArgumentNullException(nameof(eventStream));
            this.disposeEventStream = disposeEventStream;
        }

        public Task<EventId> Commit(Transaction transaction)
        {
            return WithWriteLock(() => eventStream.Commit(transaction));
        }

        public Task<ISlicedEnumerable<Event>> Events(EventId after = default(EventId), EventId before = default(EventId))
        {
            return WithReadLock(() => eventStream.Events(after, before));
        }

        public Task<IDisposable> OnCommit(Action action)
        {
            return eventStream.OnCommit(action);
        }

        protected override async Task DisposeManagedResources()
        {
            await base.DisposeManagedResources();
            if (disposeEventStream) eventStream.Dispose();
            eventStream = null;
        }
    }
}