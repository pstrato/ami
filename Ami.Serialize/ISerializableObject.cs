using System.IO;
using System.Threading.Tasks;

namespace Ami.Serialize 
{
    /// <summary> A serializable object. </summary>
    public interface ISerializableObject
    {
        /// <summary> Read this object using the provided reader. </summary>
        Task ReadFrom(IReader reader);

        /// <summary> Write this object using the provided writer. </summary>
        Task WriteTo(IWriter writer);
    }
}