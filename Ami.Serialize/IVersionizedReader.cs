using System;
using System.Threading.Tasks;

namespace Ami.Serialize
{
    /// <summary> Data reader given on deserialization once version has been read. </summary>
    public interface IVersionizedReader : IDisposable
    {
        /// <summary> Version of object being read. </summary>
        uint Version { get; }

        /// <summary> Read a serializable object from this reader. </summary>
        Task<V> Read<V>(string name) where V : ISerializableObject;

        /// <summary> Read a nullable serializable object from this reader. </summary>
        Task<V?> ReadNullable<V>(string name) where V : struct, ISerializableObject;

        /// <summary> Read a serializable array of objects from this reader. </summary>
        Task<V[]> ReadArray<V>(string name) where V : ISerializableObject;

        /// <summary> Read one byte. </summary>
        Task<byte> ReadByte(string name);

        /// <summary> Read a byte array. </summary>
        Task<byte[]> ReadByteArray(string name);

        /// <summary> Read one int. </summary>
        Task<int> ReadInt(string name);

        /// <summary> Read one long. </summary>
        Task<long> ReadLong(string name);

        /// <summary> Read one date time. </summary>
        Task<DateTime> ReadDateTime(string name);

        /// <summary> Read a string. </summary>
        Task<string> ReadString(string name);
    }
}