using System;
using System.Threading.Tasks;

namespace Ami.Serialize
{
    /// <summary> Data writer given on serialization once version has been specified. </summary>
    public interface IVersionizedWriter : IDisposable
    {
        /// <summary> Write a serializable object to this writer. </summary>
        Task Write<V>(string name, V value) where V : ISerializableObject;

        /// <summary> Write a nullable serializable object to this writer. </summary>
        Task WriteNullable<V>(string name, V? value) where V : struct, ISerializableObject;

        /// <summary> Write a nullable serializable object to this writer. </summary>
        Task WriteArray<V>(string name, V[] value) where V : ISerializableObject;

        /// <summary> Write one byte. </summary>
        Task WriteByte(string name, byte value);

        /// <summary> Write a byte array. </summary>
        Task WriteByteArray(string name, byte[] value);

        /// <summary> Write one int. </summary>
        Task WriteInt(string name, int value);

        /// <summary> Write one long. </summary>
        Task WriteLong(string name, long value);

        /// <summary> Write one date time. </summary>
        Task WriteDateTime(string name, DateTime value);

        /// <summary> Write a string. </summary>
        Task WriteString(string name, string value);
    }
}