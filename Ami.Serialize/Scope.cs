using System;
using System.Linq;
using Ami.Core;

namespace Ami.Serialize
{
    /// <summary> Identifier of a serialization scope. </summary>
    public struct Scope<K, T> : IEquatable<Scope<K, T>> where K : IKey<K> where T : ISerializableObject
    {

        public Scope(string id)
        {
            Id = id;
            Id.AssertHasOnlyInternalCharSet(nameof(Id));
        }

        /// <summary> Scope identifier. </summary>
        public string Id { get; }

        public bool Equals(Scope<K, T> other) => string.CompareOrdinal(Id, other.Id) == 0;

        public override bool Equals(object obj) => obj is Scope<K, T> s && Equals(s);

        public override int GetHashCode() => (Id ?? "").GetHashCode();
    }
}