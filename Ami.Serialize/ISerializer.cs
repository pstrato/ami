using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Ami.Core;

namespace Ami.Serialize
{
    /// <summary> Serialization interface. </summary>
    public interface ISerializer
    {
        /// <summary> Get value associated to a key. </summary>
        Task<T> Read<K, T>(Scope<K, T> scope, IKey<K> key) where K: IKey<K> where T: ISerializableObject;

        /// <summary> Set value associated to a key. </summary>
        Task Write<K, T>(Scope<K, T> scope, IKey<K> key, T value) where K: IKey<K> where T: ISerializableObject;

        /// <summary> Remove a key and its value from a scope. </summary>
        /// <returns> True if key was removed. </returns>
        Task<bool> Remove<K, T>(Scope<K, T> scope, IKey<K> key) where K: IKey<K> where T: ISerializableObject;
        
        /// <summary> Enumerate sub groups of a given base group included in a range. </summary>
        Task<IEnumerable<Id<Group, K>>> Groups<K, T>(Scope<K, T> scope, Id<Group, K> group = default(Id<Group, K>), Range<Id<Group, K>> range = default(Range<Id<Group, K>>)) where K: IKey<K> where T: ISerializableObject;

        /// <summary> Enumerate names of a given base group included in a range. </summary>
        Task<IEnumerable<Id<Name, K>>> Names<K, T>(Scope<K, T> scope, Id<Group, K> group = default(Id<Group, K>), Range<Id<Name, K>> range = default(Range<Id<Name, K>>)) where K: IKey<K> where T: ISerializableObject;
    }
}