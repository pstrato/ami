using System.Threading.Tasks;

namespace Ami.Serialize
{
    /// <summary> Data writer given on serialization. </summary>
    public interface IWriter
    {
        /// <summary> Set version of serializable object to be written. </summary>
        Task<IVersionizedWriter> SetVersion(uint version);
    }
}