using System.Threading.Tasks;

namespace Ami.Serialize
{
    /// <summary> Data reader given on deserialization. </summary>
    public interface IReader
    {
        /// <summary> Get version of object being read. </summary>
        Task<IVersionizedReader> GetVersion();
    }
}