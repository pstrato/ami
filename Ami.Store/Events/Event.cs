using System;
using System.Collections.Generic;
using Ami.Store.Events.Transactions;

namespace Ami.Store.Events {

    /// <summary> Storage event. </summary>
    public struct Event {

        EventId id;

        public Event(EventId id, Transaction transaction) {
            this.id = id;
            Transaction = transaction ?? throw new ArgumentNullException(nameof(transaction));
        }

        /// <summary> Event id. </summary>
        public EventId Id => id;

        /// <summary> Committed transaction. </summary>
        public Transaction Transaction { get; }
    }
}