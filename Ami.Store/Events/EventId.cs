using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Ami.Core;
using Ami.Serialize;

namespace Ami.Store.Events
{
    /// <summary> Storage event identifier. </summary>
    public struct EventId : IKey<EventId>, ISerializableObject
    {
        /// <summary> Null event id. </summary>
        public static EventId Null;

        DateTime timestamp;

        string name;

        string group;

        public EventId(DateTime timestamp)
        {
            this.timestamp = timestamp.ToUniversalTime();
            name = this.timestamp.ToString("o");
            group = "";
        }

        public EventId(Id<Group, EventId> group, Id<Name, EventId> name): this(DateTime.Parse(name.Value))
        {

        }

        /// <summary> Event timestamp. </summary>
        public DateTime Timestamp => timestamp;

        public override string ToString() => $"{group}{name}";

        static public EventId Parse(string timestamp) => new EventId(DateTime.Parse(timestamp ?? throw new ArgumentNullException(nameof(timestamp))));

        public static EventId ForNow => new EventId(DateTime.UtcNow);

        public Id<Group, EventId> Group => new Id<Group, EventId>(group);

        public Id<Name, EventId> Name => new Id<Name, EventId>(name);

        public void AssertIsValid()
        {
            if (Timestamp == default(DateTime)) throw new ArgumentException("EventId has not been specified");
        }

        async Task ISerializableObject.ReadFrom(IReader reader)
        {
            using (var vr = await reader.GetVersion())
            {
                if (vr.Version != 0) throw new InvalidDataException("Unknown version");
                timestamp = await vr.ReadDateTime("Timestamp");
            }
        }

        async Task ISerializableObject.WriteTo(IWriter writer)
        {
            using (var vw = await writer.SetVersion(0))
            {
                await vw.WriteDateTime("Timestampt", timestamp);
            }
        }
    }
}