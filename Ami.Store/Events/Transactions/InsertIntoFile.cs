using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Ami.Store.Resources;
using File = Ami.Store.Resources.File;

namespace Ami.Store.Events.Transactions
{
    using File=Ami.Store.Resources.File;

    /// <summary> Insert content into file. </summary>
    public sealed class InsertIntoFile : FileContentTransaction
    {

        public InsertIntoFile(Path<File> path, EventId? lastEventId, int offset, params byte[] data)
        {
            path.AssertIsNotDefault();
            if (offset < 0) throw new ArgumentException("Offset cannot be negative");
            if (data == null) throw new ArgumentNullException(nameof(data));
            if (data.Length == 0) throw new ArgumentException("Data cannot be empty");
            Path = path;
            LastEventId = lastEventId;
            Offset = offset;
            Data = data;
        }

        /// <summary> Path of the file where data is inserted. </summary>
        public Path<File> Path { get; private set; }

        /// <summary> Identifier of the last know event affecting this file or null if this affects a new file. </summary>
        public EventId? LastEventId { get; private set; }

        /// <summary> Offset in file where data is inserted. </summary>
        public int Offset { get; private set; }

        /// <summary> Data inserted. </summary>
        public byte[] Data { get; private set; }

        protected internal override Task<bool> Apply(Context context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            return context.OnInsertIntoFile(this);
        }

        public override async Task ApplyTo(Stream stream)
        {
            if (stream == null) throw new ArgumentNullException(nameof(stream));
            var writeBuffer = new byte[Math.Max(Data.Length, 1024)];
            Array.Copy(Data, writeBuffer, Data.Length);
            var bytesToWrite = Data.Length;
            var readBuffer = new byte[writeBuffer.Length];
            stream.Position = Offset;
            var finalLength = stream.Length + Data.Length;
            do
            {
                var readBytes = await stream.ReadAsync(readBuffer, 0, readBuffer.Length);
                stream.Position -= readBytes;
                await stream.WriteAsync(writeBuffer, 0, bytesToWrite);
                bytesToWrite = readBytes;
                var tmpBuffer = writeBuffer;
                writeBuffer = readBuffer;
                readBuffer = tmpBuffer;
            } while (stream.Length < finalLength);
        }

        protected override async Task ReadFrom(Serialize.IReader reader)
        {
            using (var vr = await reader.GetVersion())
            {
                if (vr.Version != 0) throw new InvalidDataException("Unkown version");
                Offset = await vr.ReadInt("Offset");
                LastEventId = await vr.ReadNullable<EventId>("LastEventId");
                Path = await vr.Read<Path<File>>("Path");
                Data = await vr.ReadByteArray("Data");
            }
        }

        protected override async Task WriteTo(Serialize.IWriter writer)
        {
            using (var vw = await writer.SetVersion(0)) 
            {
                await vw.WriteInt("Offset", Offset);
                await vw.WriteNullable("LastEventId", LastEventId);
                await vw.Write("Path", Path);
                await vw.WriteByteArray("Data", Data);
            }
        }
    }
}