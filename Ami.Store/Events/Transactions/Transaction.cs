using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Ami.Serialize;

namespace Ami.Store.Events.Transactions
{

    /// <summary> Storage transaction. </summary>
    public abstract class Transaction: ISerializableObject {

        /// <summary> Apply this transaction on the given context. </summary>
        protected internal abstract Task<bool> Apply(Context context);

        protected abstract Task ReadFrom(IReader reader);

        Task ISerializableObject.ReadFrom(IReader reader) => ReadFrom(reader);

        protected abstract Task WriteTo(IWriter writer);

        Task ISerializableObject.WriteTo(IWriter writer) => WriteTo(writer);
    }
}