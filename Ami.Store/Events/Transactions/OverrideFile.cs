using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Ami.Store.Resources;

namespace Ami.Store.Events.Transactions
{
    using File = Ami.Store.Resources.File;

    /// <summary> Override the content of a file. </summary>
    public sealed class OverrideFile : FileContentTransaction
    {

        public OverrideFile(Path<File> path, EventId lastEventId, params byte[] data)
        {
            path.AssertIsNotDefault();
            lastEventId.AssertIsValid();
            if (data == null) throw new ArgumentNullException(nameof(data));
            if (data.Length == 0) throw new ArgumentException("Data cannot be empty");
            Path = path;
            LastEventId = lastEventId;
            Data = data;
        }

        /// <summary> Path of the file to override. </summary>
        public Path<File> Path { get; private set; }

        /// <summary> Identifier of the last know event affecting this file. </summary>
        public EventId LastEventId { get; private set; }

        /// <summary> New content of the file. </summary>
        public byte[] Data { get; private set; }

        protected internal override Task<bool> Apply(Context context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            return context.OnOverrideFile(this);
        }

        public override async Task ApplyTo(Stream stream)
        {
            if (stream == null) throw new ArgumentNullException(nameof(stream));
            stream.Position = 0;
            await stream.WriteAsync(Data, 0, Data.Length);
            stream.SetLength(Data.Length);
        }

        protected override async Task ReadFrom(Serialize.IReader reader)
        {
            using (var vr = await reader.GetVersion())
            {
                if (vr.Version != 0) throw new InvalidDataException("Unkown version");
                LastEventId = await vr.Read<EventId>("LastEventId");
                Path = await vr.Read<Path<File>>("Path");
                Data = await vr.ReadByteArray("Data");
            }
        }

        protected override async Task WriteTo(Serialize.IWriter writer)
        {
            using (var vw = await writer.SetVersion(0))
            {
                await vw.Write("LastEventId", LastEventId);
                await vw.Write("Path", Path);
                await vw.WriteByteArray("Data", Data);
            }
        }
    }
}