using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Ami.Store.Resources;

namespace Ami.Store.Events.Transactions
{
    using File=Ami.Store.Resources.File;

    /// <summary> Create a new file. </summary>
    public sealed class CreateFile : Transaction
    {

        public CreateFile(Path<File> path)
        {
            path.AssertIsNotDefault();
            Path = path;
        }

        /// <summary> Path of the file to create. </summary>
        public Path<File> Path { get; private set; }

        protected internal override Task<bool> Apply(Context context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            return context.OnCreateFile(this);
        }

        protected override async Task ReadFrom(Serialize.IReader reader)
        {
            using (var vr = await reader.GetVersion())
            {
                if (vr.Version != 0) throw new InvalidDataException("Unkown version");
                Path = await vr.Read<Path<File>>("Path");
            }
        }

        protected override async Task WriteTo(Serialize.IWriter writer)
        {
            using (var vw = await writer.SetVersion(0))
            {
                await vw.Write("Path", Path);
            }
        }
    }
}