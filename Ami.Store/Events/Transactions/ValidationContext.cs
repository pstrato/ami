using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Ami.Core;
using Ami.Store.Resources;

namespace Ami.Store.Events.Transactions
{

    /// <summary> Transaction validation context. </summary>
    public class ValidationContext : Context
    {
        /// <summary> Access to a file given its path. </summary>
        public delegate Task<File> FileAccessor(Path<File> path);

        readonly FileAccessor getFile;

        StringBuilder errors;

        HashSet<Path<File>> withError;

        EventId currentEventId;

        public ValidationContext(EventId currentEventId, FileAccessor getFile)
        {
            currentEventId.AssertIsValid();
            this.currentEventId = currentEventId;
            this.getFile = getFile ?? throw new ArgumentNullException(nameof(getFile));
            ChangedFiles = new Dictionary<Path<File>, File>();
        }

        /// <summary> Current event id. </summary>
        public EventId CurrentEventId => currentEventId;

        Task<File> GetFile(Path<File> path)
        {
            if (ChangedFiles.TryGetValue(path, out var file)) return Task.FromResult(file);
            return getFile(path);
        }

        void SetFile(ref File file)
        {
            ChangedFiles[file.Path] = file;
        }

        /// <summary> Affected files and their new state. </summary>
        public Dictionary<Path<File>, File> ChangedFiles { get; }

        /// <summary> Raise an error from the given transaction for the file. </summary>
        protected void AddError(Transaction transaction, ref Path<File> path, string error)
        {
            if (withError == null) withError = new HashSet<Path<File>>();
            if (errors == null) errors = new StringBuilder();
            withError.Add(path);
            errors.AppendLine($"{transaction.GetType().Name} {path}: {error}");
        }

        /// <summary> Check if an error has already been raised on a file. </summary>
        bool HasError(ref Path<File> file) => withError != null && withError.Contains(file);

        /// <summary> Throw an exception if any errors have been raised. </summary>
        public void ThrowErrors()
        {
            if (errors != null) throw new InvalidOperationException(errors.ToString());
        }

        bool AssertHasLastEventId(Transaction transaction, ref File file, ref EventId lastEventId)
        {
            var fileLastEventId = file.LastEventId;
            if (KeyComparer<EventId>.AreEqual(ref fileLastEventId, ref EventId.Null))
            {
                var path = file.Path;
                AddError(transaction, ref path, $"File does not exist");
                return false;
            }
            if (!KeyComparer<EventId>.AreEqual(ref fileLastEventId, ref lastEventId))
            {
                var path = file.Path;
                AddError(transaction, ref path, $"File last event id {fileLastEventId} does not match {lastEventId}");
                return false;
            }
            return true;
        }

        async Task<bool> AssertHasNoLastEventId(Transaction transaction, Path<File> path)
        {
            var file = await GetFile(path);
            var fileLastEventId = file.LastEventId;
            if (!KeyComparer<EventId>.AreEqual(ref fileLastEventId, ref EventId.Null))
            {
                AddError(transaction, ref path, "File exist");
                return false;
            }
            return true;
        }

        bool AssertHasSufficientSizeForOffset(Transaction transaction, ref File file, long offset)
        {
            var size = file.Size;
            if (size < offset)
            {
                var path = file.Path;
                AddError(transaction, ref path, $"File has size {size}, offset {offset} is out of bounds");
                return false;
            }
            return true;
        }

        bool AssertHasSufficientSizeForLength(Transaction transaction, ref File file, long offset, long length)
        {
            var size = file.Size;
            if (size < offset + length)
            {
                var path = file.Path;
                AddError(transaction, ref path, $"File has size {size}, offset {offset} + length {length} = {offset + length} is out of bounds");
                return false;
            }
            return true;
        }

        /// <summary> Assert this transaction can proceed. </summary>
        public void AssertCanApply(Transaction transaction)
        {
            if (transaction == null) throw new ArgumentNullException(nameof(transaction));
            transaction.Apply(this);
            ThrowErrors();
        }

        public override async Task<bool> OnAppendToFile(AppendToFile transaction)
        {
            if (transaction == null) throw new ArgumentNullException(nameof(transaction));
            var path = transaction.Path;
            if (HasError(ref path)) return false;
            var file = await GetFile(path);
            var transactionLastEventId = transaction.LastEventId.HasValue ? transaction.LastEventId.Value : currentEventId;
            if (!AssertHasLastEventId(transaction, ref file, ref transactionLastEventId)) return false;
            file = new File(ref path, ref currentEventId, file.Size + transaction.Data.Length);
            SetFile(ref file);
            return true;
        }

        public override async Task<bool> OnCreateFile(CreateFile transaction)
        {
            if (transaction == null) throw new ArgumentNullException(nameof(transaction));
            var path = transaction.Path;
            if (HasError(ref path)) return false;
            if (!await AssertHasNoLastEventId(transaction, path)) return false;
            var file = new File(ref path, ref currentEventId, 0);
            SetFile(ref file);
            return true;
        }

        public override async Task<bool> OnDeleteFile(DeleteFile transaction)
        {
            if (transaction == null) throw new ArgumentNullException(nameof(transaction));
            var path = transaction.Path;
            if (HasError(ref path)) return false;
            var file = await GetFile(path);
            var transactionLastEventId = transaction.LastEventId;
            if (!AssertHasLastEventId(transaction, ref file, ref transactionLastEventId)) return false;
            file = new File(ref path, ref EventId.Null, 0);
            SetFile(ref file);
            return true;
        }

        public override async Task<bool> OnInsertIntoFile(InsertIntoFile transaction)
        {
            if (transaction == null) throw new ArgumentNullException(nameof(transaction));
            var path = transaction.Path;
            if (HasError(ref path)) return false;
            var file = await GetFile(path);
            var transactionLastEventId = transaction.LastEventId.HasValue ? transaction.LastEventId.Value : currentEventId;
            if (!AssertHasLastEventId(transaction, ref file, ref transactionLastEventId)) return false;
            if (!AssertHasSufficientSizeForOffset(transaction, ref file, transaction.Offset)) return false;
            file = new File(ref path, ref currentEventId, file.Size + transaction.Data.Length);
            SetFile(ref file);
            return true;
        }

        public override async Task<bool> OnOverrideFile(OverrideFile transaction)
        {
            if (transaction == null) throw new ArgumentNullException(nameof(transaction));
            var path = transaction.Path;
            if (HasError(ref path)) return false;
            var file = await GetFile(path);
            var transactionLastEventId = transaction.LastEventId;
            if (!AssertHasLastEventId(transaction, ref file, ref transactionLastEventId)) return false;
            file = new File(ref path, ref currentEventId, transaction.Data.Length);
            SetFile(ref file);
            return true;
        }

        public override async Task<bool> OnRemoveFromFile(RemoveFromFile transaction)
        {
            if (transaction == null) throw new ArgumentNullException(nameof(transaction));
            var path = transaction.Path;
            if (HasError(ref path)) return false;
            var file = await GetFile(path);
            var transactionLastEventId = transaction.LastEventId;
            if (!AssertHasLastEventId(transaction, ref file, ref transactionLastEventId)) return false;
            if (!AssertHasSufficientSizeForOffset(transaction, ref file, transaction.Offset)) return false;
            if (!AssertHasSufficientSizeForLength(transaction, ref file, transaction.Offset, transaction.Length)) return false;
            file = new File(ref path, ref currentEventId, file.Size - transaction.Length);
            SetFile(ref file);
            return true;
        }

        public override async Task<bool> OnReplaceIntoFile(ReplaceIntoFile transaction)
        {
            if (transaction == null) throw new ArgumentNullException(nameof(transaction));
            var path = transaction.Path;
            if (HasError(ref path)) return false;
            var file = await GetFile(path);
            var transactionLastEventId = transaction.LastEventId;
            if (!AssertHasLastEventId(transaction, ref file, ref transactionLastEventId)) return false;
            if (!AssertHasSufficientSizeForOffset(transaction, ref file, transaction.Offset)) return false;
            if (!AssertHasSufficientSizeForLength(transaction, ref file, transaction.Offset, transaction.Data.Length)) return false;
            file = new File(ref path, ref currentEventId, file.Size);
            SetFile(ref file);
            return true;
        }

        public override async Task<bool> OnTransactionGroup(TransactionGroup transaction)
        {
            if (transaction == null) throw new ArgumentNullException(nameof(transaction));
            var allApplied = true;
            for (var i = 0; i < transaction.Transactions.Length; ++i)
            {
                allApplied &= await transaction.Transactions[i].Apply(this);
            }
            return allApplied;
        }
    }
}