using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Ami.Store.Resources;

namespace Ami.Store.Events.Transactions
{
    using File = Ami.Store.Resources.File;

    /// <summary> Group of transactions applied together. </summary>
    public sealed class TransactionGroup : Transaction
    {

        public TransactionGroup(params Transaction[] transactions)
        {
            if (transactions == null) throw new ArgumentNullException(nameof(transactions));
            if (transactions.Length == 0) throw new ArgumentException("Transaction cannot be empty");
            Transactions = transactions;
        }

        /// <summary> Transactions to apply. </summary>
        public Transaction[] Transactions { get; private set; }

        protected internal override Task<bool> Apply(Context context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            return context.OnTransactionGroup(this);
        }

        protected override async Task ReadFrom(Serialize.IReader reader)
        {
            using (var vr = await reader.GetVersion())
            {
                if (vr.Version != 0) throw new InvalidDataException("Unkown version");
                Transactions = await vr.ReadArray<Transaction>("Transactions");
            }
        }

        protected override async Task WriteTo(Serialize.IWriter writer)
        {
            using (var vw = await writer.SetVersion(0))
            {
                await vw.WriteArray("Transactions", Transactions);
            }
        }
    }
}