using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Ami.Store.Resources;

namespace Ami.Store.Events.Transactions
{
    using File = Ami.Store.Resources.File;

    /// <summary> Append content to file. </summary>
    public sealed class AppendToFile : FileContentTransaction
    {

        public AppendToFile(Path<File> path, EventId? lastEventId, params byte[] data)
        {
            path.AssertIsNotDefault();
            if (data == null) throw new ArgumentNullException(nameof(data));
            if (data.Length == 0) throw new ArgumentException("Data cannot be empty");
            Path = path;
            LastEventId = lastEventId;
            Data = data;
        }

        /// <summary> Path of the file where data is inserted. </summary>
        public Path<File> Path { get; private set; }

        /// <summary> Identifier of the last know event affecting this file or null if this affects a new file. </summary>
        public EventId? LastEventId { get; private set; }

        /// <summary> Data inserted. </summary>
        public byte[] Data { get; private set; }

        protected internal override Task<bool> Apply(Context context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            return context.OnAppendToFile(this);
        }

        public override async Task ApplyTo(Stream stream)
        {
            if (stream == null) throw new ArgumentNullException(nameof(stream));
            stream.Position = stream.Length;
            await stream.WriteAsync(Data, 0, Data.Length);
        }

        protected override async Task ReadFrom(Serialize.IReader reader)
        {
            using (var vr = await reader.GetVersion())
            {
                if (vr.Version != 0) throw new InvalidDataException("Unkown version");
                LastEventId = await vr.ReadNullable<EventId>("LastEventId");
                Path = await vr.Read<Path<File>>("Path");
                Data = await vr.ReadByteArray("Data");
            }
        }

        protected override async Task WriteTo(Serialize.IWriter writer)
        {
            using (var vw = await writer.SetVersion(0))
            {
                await vw.WriteNullable("LastEventId", LastEventId);
                await vw.Write("Path", Path);
                await vw.WriteByteArray("Data", Data);
            }
        }
    }
}