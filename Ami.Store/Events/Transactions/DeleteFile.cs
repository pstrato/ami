using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Ami.Store.Resources;

namespace Ami.Store.Events.Transactions
{
    using File=Ami.Store.Resources.File;

    /// <summary> Delete an existing file. </summary>
    public sealed class DeleteFile : Transaction
    {

        public DeleteFile(Path<File> path, EventId lastEventId)
        {
            path.AssertIsNotDefault();
            lastEventId.AssertIsValid();
            Path = path;
            LastEventId = lastEventId;
        }

        /// <summary> Path of the file to delete. </summary>
        public Path<File> Path { get; private set; }

        /// <summary> Identifier of the last know event affecting this file. </summary>
        public EventId LastEventId { get; private set; }

        protected internal override Task<bool> Apply(Context context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            return context.OnDeleteFile(this);
        }

        protected override async Task ReadFrom(Serialize.IReader reader)
        {
            using (var vr = await reader.GetVersion())
            {
                if (vr.Version != 0) throw new InvalidDataException("Unkown version");
                LastEventId = await vr.Read<EventId>("LastEventId");
                Path = await vr.Read<Path<File>>("Path");
            }
        }

        protected override async Task WriteTo(Serialize.IWriter writer)
        {
            using (var vw = await writer.SetVersion(0))
            {
                await vw.Write("LastEventId", LastEventId);
                await vw.Write("Path", Path);
            }
        }
    }
}