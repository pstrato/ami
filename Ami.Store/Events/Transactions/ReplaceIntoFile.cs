using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Ami.Store.Resources;

namespace Ami.Store.Events.Transactions
{
    using File = Ami.Store.Resources.File;

    /// <summary> Replace existing content into an existing file. </summary>
    public sealed class ReplaceIntoFile : FileContentTransaction
    {

        public ReplaceIntoFile(Path<File> path, EventId lastEventId, int offset, params byte[] data)
        {
            path.AssertIsNotDefault();
            lastEventId.AssertIsValid();
            if (offset < 0) throw new ArgumentException("Offset cannot be negative");
            if (data == null) throw new ArgumentNullException(nameof(data));
            if (data.Length == 0) throw new ArgumentException("Data cannot be empty");
            Path = path;
            LastEventId = lastEventId;
            Offset = offset;
            Data = data;
        }

        /// <summary> Path of the file where data is replaced. </summary>
        public Path<File> Path { get; private set; }

        /// <summary> Identifier of the last know event affecting this file. </summary>
        public EventId LastEventId { get; private set; }

        /// <summary> Offset in file where data is replaced. </summary>
        public int Offset { get; private set; }

        /// <summary> New data to replace with in file. </summary>
        public byte[] Data { get; private set; }

        protected internal override Task<bool> Apply(Context context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            return context.OnReplaceIntoFile(this);
        }

        public override async Task ApplyTo(Stream stream)
        {
            if (stream == null) throw new ArgumentNullException(nameof(stream));
            stream.Position = Offset;
            await stream.WriteAsync(Data, 0, Data.Length);
        }

        protected override async Task ReadFrom(Serialize.IReader reader)
        {
            using (var vr = await reader.GetVersion())
            {
                if (vr.Version != 0) throw new InvalidDataException("Unkown version");
                Offset = await vr.ReadInt("Offset");
                LastEventId = await vr.Read<EventId>("LastEventId");
                Path = await vr.Read<Path<File>>("Path");
                Data = await vr.ReadByteArray("Data");
            }
        }

        protected override async Task WriteTo(Serialize.IWriter writer)
        {
            using (var vw = await writer.SetVersion(0)) 
            {
                await vw.WriteInt("Offset", Offset);
                await vw.Write("LastEventId", LastEventId);
                await vw.Write("Path", Path);
                await vw.WriteByteArray("Data", Data);
            }
        }
    }
}