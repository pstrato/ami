using System.IO;
using System.Threading.Tasks;

namespace Ami.Store.Events.Transactions {

    /// <summary> Transactions modifying file content. </summary>
    public abstract class FileContentTransaction: Transaction {

        /// <summary> Apply the transaction to the stream. </summary>
        public abstract Task ApplyTo(Stream stream);
    }
}