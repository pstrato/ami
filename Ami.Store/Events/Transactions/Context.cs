using System.Threading.Tasks;

namespace Ami.Store.Events.Transactions {

    /// <summary> Transaction context. </summary>
    public abstract class Context {

        /// <summary> Notify context of a AppendToFile transaction. </summary>
        /// <returns> True if transaction was applied. </returns>
        public abstract Task<bool> OnAppendToFile(AppendToFile transaction);

        /// <summary> Notify context of a CreateFile transaction. </summary>
        /// <returns> True if transaction was applied. </returns>
        public abstract Task<bool> OnCreateFile(CreateFile transaction);

        /// <summary> Notify context of a DeleteFile transaction. </summary>
        /// <returns> True if transaction was applied. </returns>
        public abstract Task<bool> OnDeleteFile(DeleteFile transaction);

        /// <summary> Notify context of a InsertIntoFile transaction. </summary>
        /// <returns> True if transaction was applied. </returns>
        public abstract Task<bool> OnInsertIntoFile(InsertIntoFile transaction);

        /// <summary> Notify context of a OverrideFile transaction. </summary>
        /// <returns> True if transaction was applied. </returns>
        public abstract Task<bool> OnOverrideFile(OverrideFile transaction);

        /// <summary> Notify context of a RemoveFromFile transaction. </summary>
        /// <returns> True if transaction was applied. </returns>
        public abstract Task<bool> OnRemoveFromFile(RemoveFromFile transaction);

        /// <summary> Notify context of a ReplaceIntoFile transaction. </summary>
        /// <returns> True if transaction was applied. </returns>
        public abstract Task<bool> OnReplaceIntoFile(ReplaceIntoFile transaction);

        /// <summary> Notify context of a transaction group. </summary>
        /// <returns> True if transaction was applied. </returns>
        public abstract Task<bool> OnTransactionGroup(TransactionGroup transaction);
    }
}