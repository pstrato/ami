using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Ami.Store.Resources;

namespace Ami.Store.Events.Transactions
{
    using File = Ami.Store.Resources.File;

    /// <summary> Remove data from an existing file. </summary>
    public sealed class RemoveFromFile : FileContentTransaction
    {

        public RemoveFromFile(Path<File> path, EventId lastEventId, int offset, int length)
        {
            path.AssertIsNotDefault();
            lastEventId.AssertIsValid();
            if (offset < 0) throw new ArgumentException("Offset cannot be negative");
            if (length <= 0) throw new ArgumentException("Length cannot be negative or zero");
            Path = path;
            LastEventId = lastEventId;
            Offset = offset;
            Length = length;
        }

        /// <summary> Path of the file where data is removed. </summary>
        public Path<File> Path { get; private set; }

        /// <summary> Identifier of the last know event affecting this file. </summary>
        public EventId LastEventId { get; private set; }

        /// <summary> Offset in file where data is removed. </summary>
        public int Offset { get; private set; }

        /// <summary> Length of data to remove. </summary>
        public int Length { get; private set; }

        protected internal override Task<bool> Apply(Context context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            return context.OnRemoveFromFile(this);
        }

        public override async Task ApplyTo(Stream stream)
        {
            if (stream == null) throw new ArgumentNullException(nameof(stream));

            var buffer = new byte[Math.Max(Length, 1024)];

            var readOffset = Offset + Length;

            do
            {
                stream.Position = readOffset;
                var readBytes = await stream.ReadAsync(buffer, 0, buffer.Length);
                stream.Position = readOffset - Length;
                await stream.WriteAsync(buffer, 0, readBytes);
                readOffset += readBytes;
            } while (readOffset < stream.Length);
            stream.SetLength(stream.Length - Length);
        }

        protected override async Task ReadFrom(Serialize.IReader reader)
        {
            using (var vr = await reader.GetVersion())
            {
                if (vr.Version != 0) throw new InvalidDataException("Unkown version");
                Offset = await vr.ReadInt("Offset");
                Length = await vr.ReadInt("Length");
                LastEventId = await vr.Read<EventId>("LastEventId");
                Path = await vr.Read<Path<File>>("Path");
            }
        }

        protected override async Task WriteTo(Serialize.IWriter writer)
        {
            using (var vw = await writer.SetVersion(0))
            {
                await vw.WriteInt("Offset", Offset);
                await vw.WriteInt("Length", Length);
                await vw.Write("LastEventId", LastEventId);
                await vw.Write("Path", Path);
            }
        }
    }
}