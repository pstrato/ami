using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Ami.Core;
using Ami.Store.Events.Transactions;

namespace Ami.Store.Events {

    /// <summary> Storage event stream interface. </summary>
    public interface IEventStream: IDisposable {

        /// <summary> Commit a transaction on event stream. </summary>
        /// <returns> Task performing the commit operation. </return>
        Task<EventId> Commit(Transaction transaction);

        /// <summary> Register an action to be executed on commit. </summary>
        /// <returns> Object to dispose in order to unregister the action. </returns>
        Task<IDisposable> OnCommit(Action action);

        /// <summary> Asynchronuous list events operation on storage log. </summary>
        /// <returns> Task performing the list events operation. </return>
        Task<IEnumerable<Task<Event>>> Events(Range<Id<Name, EventId>> range = default(Range<Id<Name, EventId>>));
    }
}