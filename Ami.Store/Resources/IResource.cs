using System;
using System.Threading.Tasks;
using Ami.Store.Events;

namespace Ami.Store.Resources {

    /// <summary> Stored resource, file or folder. </summary>
    public interface IResource {

        /// <summary> Identifier of the last event affecting this resource. </summary>
        EventId LastEventId { get; }
    }

    /// <summary> Stored resource, file or folder. </summary>
    public interface IResource<T>: IResource where T: IResource<T> {

        /// <summary> Resource path. </summary>
        Path<T> Path { get; }
    }
}