using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Ami.Store.Events;
using Ami.Store.Events.Transactions;

namespace Ami.Store.Resources {

    /// <summary> Transaction application context on resources. </summary>
    public class TransactionApplicationContext : ValidationContext {

        Dictionary<Path<File>, List<FileContentTransaction>> fileTransactions;

        byte[] buffer;
        
        public TransactionApplicationContext(EventId currentEventId, FileAccessor getFile) : base(currentEventId, getFile) {
            fileTransactions = new Dictionary<Path<File>, List<FileContentTransaction>>();
            buffer = new byte[1024];
        }
        /// <summary> Apply the given transaction. </summary>
        public void Apply(Transaction transaction) {
            AssertCanApply(transaction);
        }

        public override async Task<bool> OnDeleteFile(DeleteFile transaction) {
            if (await base.OnDeleteFile(transaction)) {
                var path = transaction.Path;
                fileTransactions.Remove(path);
                return true;
            }
            return false;
        }

        public List<FileContentTransaction> GetTransactions(ref Path<File> path) {
            if (!fileTransactions.TryGetValue(path, out var transactions)) {
                fileTransactions.Add(path, transactions = new List<FileContentTransaction>());
            }
            return transactions;
        }

        public override async Task<bool> OnAppendToFile(AppendToFile transaction) {
            if (await base.OnAppendToFile(transaction)) {
                var path = transaction.Path;
                GetTransactions(ref path).Add(transaction);
                return true;
            }
            return false;
        }

        public override async Task<bool> OnInsertIntoFile(InsertIntoFile transaction) {
            if (await base.OnInsertIntoFile(transaction)) {
                var path = transaction.Path;
                GetTransactions(ref path).Add(transaction);
                return true;
            }
            return false;
        }

        public override async Task<bool> OnOverrideFile(OverrideFile transaction) {
            if (await base.OnOverrideFile(transaction)) {
                var path = transaction.Path;
                var transactions = GetTransactions(ref path);
                transactions.Clear();
                transactions.Add(transaction);
                return true;
            }
            return false;
        }

        public override async Task<bool> OnRemoveFromFile(RemoveFromFile transaction) {
            if (await base.OnRemoveFromFile(transaction)) {
                var path = transaction.Path;
                GetTransactions(ref path).Add(transaction);
                return true;
            }
            return false;
        }

        public override async Task<bool> OnReplaceIntoFile(ReplaceIntoFile transaction) {
            if (await base.OnReplaceIntoFile(transaction)) {
                var path = transaction.Path;
                GetTransactions(ref path).Add(transaction);
                return true;
            }
            return false;
        }
    }
}