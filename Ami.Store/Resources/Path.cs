using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Ami.Core;
using Ami.Serialize;

namespace Ami.Store.Resources
{
    /// <summary> Path of a resource. </summary>
    public struct Path<T> : IKey<Path<T>>, ISerializableObject where T : IResource<T>
    {
        /// <summary> All characters invalid in resource paths. </summary>
        public static readonly char[] InvalidChars;

        /// <summary> Path segment separator. </summary>
        public const char SegmentSeparator = '/';

        /// <summary> Name of root path. </summary>
        internal static readonly string RootPath = $"{SegmentSeparator}";

        static Path()
        {
            InvalidChars = System.IO.Path.GetInvalidPathChars()
                .Union(System.IO.Path.GetInvalidFileNameChars())
                .Union(new[]{
                    System.IO.Path.DirectorySeparatorChar,
                    System.IO.Path.AltDirectorySeparatorChar,
                    '.', '/', '\\', ' ', '\t', '\n', '\r', ':'
                })
                .Distinct().OrderBy(c => c).ToArray();
        }

        string resourcePath;
        
        public Path(string resourcePath) : this(resourcePath, false)
        {
        }

        internal Path(string resourcePath, bool trusted)
        {
            this.resourcePath = null;
            this.resourcePath = Validate(resourcePath, trusted);
            var lastSegementSeparator = this.resourcePath.LastIndexOf(SegmentSeparator, resourcePath.Length - 2);
            if (this.resourcePath == RootPath)
            {
                Group = default(Id<Group, Path<T>>);
                Name = default(Id<Name, Path<T>>);
            }
            else
            {
                Group = new Id<Group, Path<T>>(resourcePath.Substring(0, lastSegementSeparator + 1));
                Name = new Id<Name, Path<T>>(resourcePath.Substring(lastSegementSeparator + 1, resourcePath.Length - (lastSegementSeparator + 1) - 1));
            }
        }

        string Validate(string resourcePath, bool trusted)
        {
            if (trusted)
            {
                return resourcePath ?? RootPath;
            }
            if (string.IsNullOrWhiteSpace(resourcePath))
            {
                return RootPath;
            }
            if (resourcePath[0] != SegmentSeparator) throw new ArgumentException($"Expected {SegmentSeparator} at position 0");
            var lastWasSegmentSeparator = true;
            for (var i = 1; i < resourcePath.Length; ++i)
            {
                var c = resourcePath[i];
                if (!lastWasSegmentSeparator)
                {
                    if (c == SegmentSeparator)
                    {
                        lastWasSegmentSeparator = true;
                    }
                    else if (Array.BinarySearch(InvalidChars, c) >= 0)
                    {
                        throw new ArgumentException($"Invalid character {c} at position {i}");
                    }
                }
                else
                {
                    if (c == SegmentSeparator)
                    {
                        throw new ArgumentException($"Unexpected {SegmentSeparator} at position {i}");
                    }
                    else if (Array.BinarySearch(InvalidChars, c) >= 0)
                    {
                        throw new ArgumentException($"Invalid character {c} at position {i}");
                    }
                    else
                    {
                        lastWasSegmentSeparator = false;
                    }
                }
            }
            if (!lastWasSegmentSeparator)
            {
                if (typeof(T) == typeof(Folder)) resourcePath += SegmentSeparator;
            }
            else
            {
                if (typeof(T) == typeof(File)) throw new ArgumentException($"File path must not end with {SegmentSeparator}");
            }
            return resourcePath;
        }

        /// <summary> Resource path. </summary>
        public string ResourcePath => resourcePath ?? RootPath;

        /// <summary> Parent of this resource. </summary>
        public Path<Folder> ParentFolder => new Path<Folder>(Group.Value, trusted: true);

        public Id<Group, Path<T>> Group { get; }

        public Id<Name, Path<T>> Name { get; }

        /// <summary> Throw an exception for when a resource already exists. </summary>
        public void ThrowResourceAlreadyExists()
        {
            throw new InvalidOperationException("Resource already exists");
        }

        /// <summary> Throw an exception for when a resource is not found. </summary>
        public void ThrowResourceNotFound()
        {
            throw new InvalidOperationException("Resource not found");
        }

        public override string ToString() => ResourcePath;

        public static Path<T> operator +(Path<Folder> basePath, Path<T> relativePath)
        {
            var root = default(Path<Folder>);
            if (KeyComparer<Path<Folder>>.AreEqual(ref basePath, ref root)) return relativePath;
            return new Path<T>($"{basePath.ResourcePath}{relativePath.ResourcePath.Substring(1)}");
        }

        async Task ISerializableObject.ReadFrom(IReader reader)
        {
            using (var vr = await reader.GetVersion())
            {
                if (vr.Version != 0) throw new InvalidDataException("Unknown version");
                resourcePath = Validate(await vr.ReadString("ResourcePath"), false);
            }
        }

        async Task ISerializableObject.WriteTo(IWriter writer)
        {
            using (var vw = await writer.SetVersion(0))
            {
                await vw.WriteString("ResourcePath", resourcePath);
            }
        }
    }
}