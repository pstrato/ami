using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Ami.Store.Events;

namespace Ami.Store.Resources {

    /// <summary> Storage. </summary>
    public interface IStorage: IDisposable {

        /// <summary> Update this storage to reflect changes. </summary>
        /// <param name="eventId"> Identifier of the event to update to, or default to update to the last available event. </param>
        /// <returns> Identifier of the last event included. </summary>
        Task<EventId> Update(EventId eventId = default(EventId));

        /// <summary> Get a file from its path. </summary>
        Task<File> Get(Path<File> path);

        /// <summary> Get a folder from its path. </summary>
        Task<Folder> Get(Path<Folder> path);

        /// <summary> List files given a base path. </summary>
        Task<IEnumerable<Task<File>>> ListFiles(Path<Folder> basePath = default(Path<Folder>));

        /// <summary> List folders given a base path. </summary>
        Task<IEnumerable<Task<Folder>>> ListFolders(Path<Folder> basePath = default(Path<Folder>));

        /// <summary> Get the content of a file from its path. </summary>
        Task<Stream> Content(Path<File> path);

        /// <summary> Register an action to be executed on storage change. </summary>
        /// <returns> Object to dispose in order to unregister the action. </returns>
        Task<IDisposable> OnChange(Action action);
    }
}