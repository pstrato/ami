using System;
using System.IO;
using System.Threading.Tasks;
using Ami.Core;
using Ami.Serialize;
using Ami.Store.Events;

namespace Ami.Store.Resources
{
    /// <summary> Stored file. </summary>
    public struct File : IResource<File>, ISerializableObject
    {
        string path;

        EventId lastEventId;

        long size;

        public File(Path<File> path, EventId lastEventId, long size) : this(ref path, ref lastEventId, size) { }

        public File(ref Path<File> path, ref EventId lastEventId, long size)
        {
            path.AssertIsNotDefault();
            if (KeyComparer<EventId>.AreEqual(ref lastEventId, ref EventId.Null))
            {
                if (size != 0) throw new ArgumentException("Size must be 0 when no event id is given", nameof(size));
            }
            else
            {
                if (size < 0) throw new ArgumentException("Size must be 0 or greater when event id is given", nameof(size));
            }
            this.path = path.ToString();
            this.lastEventId = lastEventId;
            this.size = size;
        }

        public File(ref Path<File> path) : this(ref path, ref EventId.Null, 0)
        {
        }

        public Path<File> Path => new Path<File>(path, true);

        public EventId LastEventId => lastEventId;

        /// <summary> True if the file exists. </summary>
        public bool Exists => !KeyComparer<EventId>.AreEqual(ref lastEventId, ref EventId.Null);

        /// <summary> File size, in bytes. </summary>
        public long Size => size;

        public override string ToString() => $"{Path}@{LastEventId}";

        async Task ISerializableObject.ReadFrom(IReader reader)
        {
            using (var vr = await reader.GetVersion())
            {
                if (vr.Version != 0) throw new InvalidDataException("Unknown version");
                size = await vr.ReadLong("Size");
                lastEventId = await vr.Read<EventId>("LastEventId");
                path = (await vr.Read<Path<File>>("Path")).ToString();
            }
        }

        async Task ISerializableObject.WriteTo(IWriter writer)
        {
            using (var vw = await writer.SetVersion(0))
            {
                await vw.WriteLong("Size", size);
                await vw.Write("LastEventId", lastEventId);
                await vw.Write("Path", Path);
            }
        }
    }
}