using System;
using System.Collections.Generic;

namespace Ami.Store.Resources {

    /// <summary> Extension methods for <see cref="Path{T}"/>. </summary>
    public static class PathHelper {

        /// <summary> Ensure this is not root. </summary>
        public static void AssertIsNotRoot(this Path<Folder> folder) {
            if (folder.ResourcePath == Path<Folder>.RootPath) throw new ArgumentException("Resource path has not been given");
        }

        /// <summary> Ensure this is not default. </summary>
        public static void AssertIsNotDefault(this Path<File> file) {
            if (file.ResourcePath == Path<Folder>.RootPath) throw new ArgumentException("Resource path has not been given");
        }

        /// <summary> Check if a folder path includes another resource path. </summary>
        public static bool Includes<T>(this Path<Folder> folder, Path<T> resource) where T: IResource<T> {
            return resource.ResourcePath.StartsWith(folder.ResourcePath);
        }
    }
}