using System;
using System.IO;
using System.Threading.Tasks;
using Ami.Serialize;
using Ami.Store.Events;

namespace Ami.Store.Resources {
    
    /// <summary> Stored folder. </summary>
    public struct Folder: IResource<Folder>, ISerializableObject {

        string path;

        private EventId lastEventId;
        
        public Folder(Path<Folder> path, EventId lastEventId): this(ref path, ref lastEventId) {}
        
        public Folder(ref Path<Folder> path, ref EventId lastEventId) {
            this.path = path.ToString();
            this.lastEventId = lastEventId;
        }

        public Path<Folder> Path => new Path<Folder>(path, true);

        public EventId LastEventId => lastEventId;

        public override string ToString() => $"{Path}@{LastEventId}";

        async Task ISerializableObject.ReadFrom(IReader reader)
        {
            using (var vr = await reader.GetVersion())
            {
                if (vr.Version != 0) throw new InvalidDataException("Unknown version");
                lastEventId = await vr.Read<EventId>("LastEventId");
                path = (await vr.Read<Path<File>>("Path")).ToString();
            }
        }

        async Task ISerializableObject.WriteTo(IWriter writer)
        {
            using (var vw = await writer.SetVersion(0))
            {
                await vw.Write("LastEventId", lastEventId);
                await vw.Write("Path", Path);
            }
        }
    }
}