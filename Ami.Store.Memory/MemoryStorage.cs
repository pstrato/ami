using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Ami.Core;
using Ami.Core.Collections;
using Ami.Store.Events;
using Ami.Store.Resources;
using File=Ami.Store.Resources.File;

namespace Ami.Store.Resources.Memory {

    /// <summary> In-memory storage. </summary>
    public class MemoryStorage: Core.SynchronizedObject, IStorage {

        class Leaf {

            public File File { get; set; }

            public byte[] Bytes { get; set; }

            public async Task<Stream> GetStream(bool writable) {
                if (!writable) return new MemoryStream(Bytes ?? new byte[0], false);
                var stream = new MemoryStream(Bytes?.Length ?? 1024);
                if (Bytes != null) {
                    await stream.WriteAsync(Bytes, 0, Bytes.Length);
                }
                return stream;
            }
        }

        class Node {

            public Node Parent { get; set; }

            public Name<Folder> Name { get; set; }

            public Folder Folder { get; set; }

            public Dictionary<Path<File>, Leaf> Leaves { get; set; }

            public Dictionary<Name<Folder>, Node> Nodes { get; set; }

            public Node ContainerOf(ref Path<File> path) {
                var parentFolder = path.ParentFolder;
                return Get(ref parentFolder, ref EventId.Null);
            }

            public Node CreateContainerFor(ref Path<File> path, ref EventId eventId) {
                var parentFolder = path.ParentFolder;
                return Get(ref parentFolder, ref eventId);
            }

            public Leaf Get(ref Path<File> path) {
                var container = ContainerOf(ref path);
                if (container == null) return null;
                var leaves = container.Leaves;
                if (leaves == null) return null;
                if (!leaves.TryGetValue(path, out var leaf)) return null;
                return leaf;
            }

            public Leaf Set(ref File file) {
                var path = file.Path;
                if (Leaves == null) {
                    Leaves = new Dictionary<Path<File>, Leaf>();
                }
                if (!Leaves.TryGetValue(path, out var leaf)) {
                    leaf = new Leaf();
                    Leaves.Add(path, leaf);
                }
                leaf.File = file;
                Folder = new Folder(Folder.Path, file.LastEventId);
                return leaf;
            }

            public void Delete(ref Path<File> path, ref EventId eventId) {
                Leaves.Remove(path);

                if (Leaves.Count == 0) {
                    Leaves = null;
                    if (Nodes == null && Parent != null) {
                        Parent.Remove(this, ref eventId);
                        return;
                    }
                }
                Folder = new Folder(Folder.Path, eventId);
            }

            public void Remove(Node subNode, ref EventId eventId) {
                Nodes.Remove(subNode.Name);

                if (Nodes.Count == 0) {
                    Nodes = null;
                    if (Leaves == null && Parent != null) {
                        Parent.Remove(this, ref eventId);
                        return;
                    }
                }
                Folder = new Folder(Folder.Path, eventId);
            }

            public Node Get(ref Path<Folder> path, ref EventId eventId) {
                var create = !EventId.AreEquals(ref eventId, ref EventId.Null);
                var thisFolderPath = Folder.Path;
                if (Path<Folder>.AreEqual(ref thisFolderPath, ref path)) return this;

                var node = this;

                foreach (var segment in path.Segments()) {
                    var subNodes = node.Nodes;
                    if (subNodes == null) {
                        if (!create) return null;
                        node.Nodes = subNodes = new Dictionary<Name<Folder>, Node>();
                    }
                    if (!subNodes.TryGetValue(segment, out var subNode)) {
                        if (!create) return null;
                        subNode = new Node{ Parent = node, Name = segment, Folder = new Folder(node.Folder.Path + segment, eventId)};
                        subNodes.Add(segment, subNode);
                        node.Folder = new Folder(node.Folder.Path, eventId);
                    }
                    node = subNode;
                }
                return node;
            }
        }

        Connector onChange;

        IEventStream eventStream;

        IDisposable onCommit;

        EventId lastEventId;

        Node root;

        Exception syncException;

        public MemoryStorage(IEventStream eventStream) {
            this.eventStream = eventStream ?? throw new ArgumentNullException(nameof(eventStream));
            onChange = new Connector();

            root = new Node { Folder = default(Folder) };            
            onCommit = this.eventStream.OnCommit(OnCommit);
        }

        void AssertNoSyncException() {
            if (syncException != null) throw new Exception("Failed to synchronize", syncException);
        }

        void OnCommit() {
            WithWriteLock(async () => {
                AssertNoSyncException();
                try {
                    return await Update();
                } catch (Exception e) {
                    syncException = e;
                    return null as object;
                }
            });
        }

        async Task<object> Update() {
            var events = await eventStream.Events(after: lastEventId);
            foreach (var eventGroupTask in events) {
                var eventGroup = await eventGroupTask;
                foreach (var e in eventGroup) {
                    var id = e.Id;
                    var context = CreateContext(ref id);
                    context.Apply(e.Transaction);
                    lastEventId = id;
                    var changes = context.ChangedFiles;
                    foreach (var change in changes) {
                        var file = change.Value;
                        var path = file.Path;
                        if (file.LastEventId != EventId.Null) {
                            var node = root.CreateContainerFor(ref path, ref lastEventId);
                            var leaf = node.Set(ref file);
                            var transactions = context.GetTransactions(ref path);
                            if (transactions != null && transactions.Count > 0) {
                                using (var stream = await leaf.GetStream(true)) {
                                    foreach (var transaction in transactions) {
                                        transaction.ApplyTo(stream).Wait();
                                    }
                                    var bytes = stream.ReadAllBytes();
                                    leaf.Bytes = bytes.ToArray();
                                }
                            }
                        } else {
                            var node = root.ContainerOf(ref path);
                            if (node == null) throw new InvalidDataException();
                            node.Delete(ref path, ref lastEventId);
                        }
                    }
                }
            }
            return null;
        }


        TransactionApplicationContext CreateContext(ref EventId eventId) {
            return new TransactionApplicationContext(eventId,
                (ref Path<File> path, out File file) => {
                    var container = root.ContainerOf(ref path);
                    if (container == null) {
                        file = default(File);
                        return;
                    }
                    var leaves = container.Leaves;
                    if (leaves == null) {
                        file = default(File);
                        return;
                    }
                    if (!leaves.TryGetValue(path, out var leaf)) {
                        file = default(File);
                        return;
                    }
                    file = leaf.File;
                }
            );
        }

        protected override void OnDispose() {
            base.OnDispose();
            onCommit.Dispose();
            onCommit = null;
            onChange.Dispose();
            onChange = null;
            eventStream = null;
        }

        public Task<File> Get(Path<File> path) {
            return WithReadLock(() => {
                AssertNoSyncException();
                return root.Get(ref path)?.File ?? new File(ref path, ref EventId.Null, 0);
            });
        }

        public Task<Folder> Get(Path<Folder> path) {
            return WithReadLock(() => {
                AssertNoSyncException();
                return root.Get(ref path, ref EventId.Null)?.Folder ?? new Folder(ref path, ref EventId.Null);
            });
        }

        public Task<ISlicedEnumerable<File>> ListFiles(Path<Folder> basePath = default(Path<Folder>)) {
            return WithReadLock(() => {
                AssertNoSyncException();
                var node = root.Get(ref basePath, ref EventId.Null);
                if (node == null) return SlicedEnumerable<File>.Empty;
                var leaves = node.Leaves;
                if (leaves == null) return SlicedEnumerable<File>.Empty;

                var files = new LinkedList<File>();
                foreach (var leaf in leaves) {
                    files.AddLast(leaf.Value.File);
                }
                return new SlicedEnumerable<File>(Task.FromResult(files.AsEnumerable())) as ISlicedEnumerable<File>;
            });
        }

        public Task<ISlicedEnumerable<Folder>> ListFolders(Path<Folder> basePath = default(Path<Folder>)) {
            return WithReadLock(() => {
                AssertNoSyncException();
                var node = root.Get(ref basePath, ref EventId.Null);
                if (node == null) return SlicedEnumerable<Folder>.Empty;
                var subNodes = node.Nodes;
                if (subNodes == null) return SlicedEnumerable<Folder>.Empty;

                var folders = new LinkedList<Folder>();
                foreach (var subNode in node.Nodes) {
                        folders.AddLast(subNode.Value.Folder);
                }
                return new SlicedEnumerable<Resources.Folder>(Task.FromResult(folders.AsEnumerable())) as ISlicedEnumerable<Folder>;
            });
        }

        public Task<Stream> Content(Path<File> path) {
            return WithReadLock(async () => {
                AssertNoSyncException();
                var leaf = root.Get(ref path);
                if (leaf == null) {
                    path.ThrowResourceNotFound();
                }
                return await leaf.GetStream(false);
            });
        }

        public Task<IDisposable> OnChange(Action action) => onChange.Connect(action);
    }
}