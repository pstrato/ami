using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ami.Core.Collections;
using Ami.Store.Events.Transactions;
using Ami.Store.Resources;

namespace Ami.Store.Events.Memory {

    /// <summary> In memory event stream. </summary>
    public class MemoryEventStream: AbstractEventStream, IEventStream {

        List<Event> events;

        Dictionary<Path<File>, File> files;

        public MemoryEventStream() {
            events = new List<Event>();
            files = new Dictionary<Path<File>, File>();
        }

        protected override Task<ISlicedEnumerable<Event>> DoEvents(EventId after, EventId before) {
            int afterIndex;
            if (!EventId.AreEquals(ref after, ref EventId.Null)) {
                var index = events.BinarySearch(Event.TemplateForId(ref after), Event.IdComparer.Instance);
                if (index >= 0) afterIndex = index + 1;
                else afterIndex = Math.Max(0, (~index) + 1);
            } else afterIndex = 0;

            int beforeIndex;
            if (!EventId.AreEquals(ref before, ref EventId.Null)) {
                var index = events.BinarySearch(Event.TemplateForId(ref before), Event.IdComparer.Instance);
                if (index >= 0) beforeIndex = index - 1;
                else beforeIndex = Math.Min(events.Count - 1, (~index) - 1);
            } else beforeIndex = events.Count - 1;

            var count = beforeIndex - afterIndex + 1;
            if (count <= 0) return Task.FromResult(new SlicedEnumerable<Event>() as ISlicedEnumerable<Event>);

            var selection = new List<Event>(count);
            for (var i = afterIndex; i <= beforeIndex; ++i) {
                selection.Add(events[i]);
            }
            return Task.FromResult(new SlicedEnumerable<Event>(Task.FromResult(selection.AsEnumerable())) as ISlicedEnumerable<Event>);
        }
        
        protected override Task<EventId> DoCommit(Transaction transaction) {
            var id = new EventId(DateTime.UtcNow);
            var context = new ValidationContext(id, (ref Path<File> path, out File file) => files.TryGetValue(path, out file));
            context.AssertCanApply(transaction);
            events.Add(new Event(id, transaction));
            foreach (var change in context.ChangedFiles) {
                var file = change.Value;
                var path = file.Path;
                var size = file.Size;
                if (file.LastEventId != EventId.Null) {
                    files[path] = new File(ref path, ref id, size);
                } else {
                    files.Remove(path);
                }
            }
            return Task.FromResult(id);
        }

        protected override void OnDispose() {
            base.OnDispose();
            events = null;
            files = null;
        }
    }
}