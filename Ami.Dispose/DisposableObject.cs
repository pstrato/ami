﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Ami.Dispose
{
    /// <summary>
    /// Disposable object.
    /// </summary>
    public abstract class DisposableObject : IDisposable
    {
        private int disposableState;

        ~DisposableObject()
        {
            DisposeResources(false).Wait();
        }

        public bool IsUndisposed
        {
            get
            {
                return Thread.VolatileRead(ref disposableState) == 0;
            }
        }

        public void AssertIsUndisposed()
        {
            if (!IsUndisposed) throw new ObjectDisposedException(GetType().FullName);
        }

        public void Dispose()
        {
            if (Interlocked.CompareExchange(ref disposableState, 1, 0) == 0)
            {
                DisposeResources(true).Wait();
                GC.SuppressFinalize(this);
            }
        }

        async Task DisposeResources(bool disposeManagedResources)
        {
            if (disposeManagedResources) await DisposeManagedResources();
            await DisposeUnmanagedResources();
            ClearReferences();
        }

        protected virtual void ClearReferences() { }

        protected virtual Task DisposeManagedResources() => Task.CompletedTask;

        protected virtual Task DisposeUnmanagedResources() => Task.CompletedTask;
    }
}