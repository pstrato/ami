using System;
using System.Threading;
using System.Threading.Tasks;
using Ami.Dispose;

namespace Ami.Synchronize {

    /// <summary> Object allowing thread safe read/write operations. </summary>
    public abstract class SynchronizedObject: DisposableObject {

        ReaderWriterLockSlim syncLock;

        protected SynchronizedObject() {
            syncLock = new ReaderWriterLockSlim();
        }

        protected Task WithReadLock(Action action) {
            if (action == null) throw new ArgumentNullException(nameof(action));
            var syncLock = this.syncLock;
            if (syncLock == null) throw new ObjectDisposedException(nameof(SynchronizedObject));
            return Task.Run(() => {
                syncLock.EnterReadLock();
                try {
                    action();
                } finally {
                    syncLock.ExitReadLock();
                }
            });
        }

        protected Task WithReadLock(Func<Task> action) {
            if (action == null) throw new ArgumentNullException(nameof(action));
            var syncLock = this.syncLock;
            if (syncLock == null) throw new ObjectDisposedException(nameof(SynchronizedObject));
            return Task.Run(() => {
                syncLock.EnterReadLock();
                try {
                    action().Wait();
                } finally {
                    syncLock.ExitReadLock();
                }
            });
        }

        protected Task<T> WithReadLock<T>(Func<Task<T>> action) {
            if (action == null) throw new ArgumentNullException(nameof(action));
            var syncLock = this.syncLock;
            if (syncLock == null) throw new ObjectDisposedException(nameof(SynchronizedObject));
            return Task.Run(() => {
                syncLock.EnterReadLock();
                try {
                    return action().Result;
                } finally {
                    syncLock.ExitReadLock();
                }
            });
        }

        protected Task OpenReadLock() {
            var syncLock = this.syncLock;
            if (syncLock == null) throw new ObjectDisposedException(nameof(SynchronizedObject));
            return Task.Run(() => {
                syncLock.EnterReadLock();
            });
        }

        protected void CloseReadLock() {
            var syncLock = this.syncLock;
            if (syncLock == null) throw new ObjectDisposedException(nameof(SynchronizedObject));
            syncLock.ExitReadLock();            
        }

        protected Task<T> WithReadLock<T>(Func<T> action) {
            if (action == null) throw new ArgumentNullException(nameof(action));
            var syncLock = this.syncLock;
            if (syncLock == null) throw new ObjectDisposedException(nameof(SynchronizedObject));
            return Task.Run(() => {
                syncLock.EnterReadLock();
                try {
                    return action();
                } finally {
                    syncLock.ExitReadLock();
                }
            });
        }

        protected Task WithWriteLock(Action action) {
            if (action == null) throw new ArgumentNullException(nameof(action));
            var syncLock = this.syncLock;
            if (syncLock == null) throw new ObjectDisposedException(nameof(SynchronizedObject));
            return Task.Run(() => {
                syncLock.EnterWriteLock();
                try {
                    action();
                } finally {
                    syncLock.ExitWriteLock();
                }
            });
        }

        protected Task WithWriteLock(Func<Task> action) {
            if (action == null) throw new ArgumentNullException(nameof(action));
            var syncLock = this.syncLock;
            if (syncLock == null) throw new ObjectDisposedException(nameof(SynchronizedObject));
            return Task.Run(() => {
                syncLock.EnterWriteLock();
                try {
                    action().Wait();
                } finally {
                    syncLock.ExitWriteLock();
                }
            });
        }

        protected Task<T> WithWriteLock<T>(Func<Task<T>> action) {
            if (action == null) throw new ArgumentNullException(nameof(action));
            var syncLock = this.syncLock;
            if (syncLock == null) throw new ObjectDisposedException(nameof(SynchronizedObject));
            return Task.Run(() => {
                syncLock.EnterWriteLock();
                try {
                    return action().Result;
                } finally {
                    syncLock.ExitWriteLock();
                }
            });
        }

        protected Task<T> WithWriteLock<T>(Func<T> action) {
            if (action == null) throw new ArgumentNullException(nameof(action));
            var syncLock = this.syncLock;
            if (syncLock == null) throw new ObjectDisposedException(nameof(SynchronizedObject));
            return Task.Run(() => {
                syncLock.EnterWriteLock();
                try {
                    return action();
                } finally {
                    syncLock.ExitWriteLock();
                }
            });
        }
        
        protected override async Task DisposeManagedResources() {
            await base.DisposeManagedResources();
            var syncLock = this.syncLock;
            if (syncLock == null) return;
            try {
                syncLock.EnterWriteLock();
                this.syncLock = null;
            } finally {
                syncLock.ExitWriteLock();
                syncLock.Dispose();
            }
        }
    }
}