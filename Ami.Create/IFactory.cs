﻿using System;
using System.Threading.Tasks;

namespace Ami.Create
{
    /// <summary> Create objects. </summary>
    public interface IFactory
    {
        /// <summary> Create an object for the source. </summary>
        Task<T> Create<S, T>(S source);
    }
}
