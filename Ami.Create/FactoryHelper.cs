using System.Threading.Tasks;

namespace Ami.Create
{
    /// <summary> Extension methods for factories. </summary>
    public static class FactoryHelper
    {
        /// <summary> Create two objects for the source. </summary>
        public static async Task<(T1, T2)> Create<S, T1, T2>(this IFactory factory, S source)
        {
            return (await factory.Create<S, T1>(source), await factory.Create<S, T2>(source));
        }

        /// <summary> Create three objects for the source. </summary>
        public static async Task<(T1, T2, T3)> Create<S, T1, T2, T3>(this IFactory factory, S source)
        {
            return (await factory.Create<S, T1>(source), await factory.Create<S, T2>(source), await factory.Create<S, T3>(source));
        }

        /// <summary> Create four objects for the source. </summary>
        public static async Task<(T1, T2, T3, T4)> Create<S, T1, T2, T3, T4>(this IFactory factory, S source)
        {
            return (await factory.Create<S, T1>(source), await factory.Create<S, T2>(source), await factory.Create<S, T3>(source), await factory.Create<S, T4>(source));
        }
    }
}