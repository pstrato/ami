using System;

namespace Ami.Core
{
    /// <summary> Identifier. </summary>
    public struct Id<T, K> : IEquatable<Id<T, K>>, IComparable<Id<T, K>> where T: IIdentifiable
    {
        private string value;

        public Id(string value)
        {
            this.value = value;
        }

        public string Value => value ?? "";
        
        public override string ToString() => value;

        public override bool Equals(object obj) => obj is Id<T, K> p && AreEqual(ref this, ref p);

        public bool Equals(Id<T, K> p) => AreEqual(ref this, ref p);

        public override int GetHashCode() => Value.GetHashCode();

        public static bool AreEqual(ref Id<T, K> f1, ref Id<T, K> f2) => string.CompareOrdinal(f1.value, f2.value) == 0;

        public int CompareTo(Id<T, K> p) => Compare(ref this, ref p);

        public static int Compare(ref Id<T, K> f1, ref Id<T, K> f2) => string.CompareOrdinal(f1.value, f2.value);

        public static bool operator ==(Id<T, K> a, Id<T, K> b) => AreEqual(ref a, ref b);
        public static bool operator !=(Id<T, K> a, Id<T, K> b) => !AreEqual(ref a, ref b);

        public static bool operator <=(Id<T, K> a, Id<T, K> b) => Compare(ref a, ref b) <= 0;
        public static bool operator >=(Id<T, K> a, Id<T, K> b) => Compare(ref a, ref b) >= 0;

        public static bool operator <(Id<T, K> a, Id<T, K> b) => Compare(ref a, ref b) < 0;
        public static bool operator >(Id<T, K> a, Id<T, K> b) => Compare(ref a, ref b) > 0;
    }
}