using System;
using System.Linq;

namespace Ami.Core
{
    /// <summary> Set of characters allowed internally. </summary>
    public static class InternalCharSet
    {
        static char[] InvalidChars;

        static InternalCharSet(){
            InvalidChars = System.IO.Path.GetInvalidPathChars()
                .Union(System.IO.Path.GetInvalidFileNameChars())
                .Union(new []{
                    System.IO.Path.DirectorySeparatorChar,
                    System.IO.Path.AltDirectorySeparatorChar,
                    '.', '/', '\\', ' ', '\t', '\n', '\r', ':'
                })
                .Distinct().OrderBy(c => c).ToArray();
        }

        /// <summary> Assert the given value only uses characters from the internal set. </summary>
        public static void AssertUseOnlyInternalCharSet(this string value, string name)
        {
            if (value == null) return;
            if (value.IndexOfAny(InvalidChars) >= 0) throw new ArgumentOutOfRangeException(name);
        }

        /// <summary> Assert the given value is not null or empty and only uses characters from the internal set. </summary>
        public static void AssertHasOnlyInternalCharSet(this string value, string name)
        {
            if (value == null) throw new ArgumentNullException(name);
            if (value.IndexOfAny(InvalidChars) >= 0) throw new ArgumentOutOfRangeException(name);
        }
    }
}