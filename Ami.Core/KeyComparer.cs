using System;
using System.Collections.Generic;

namespace Ami.Core
{
    /// <summary> Equality and comparison of keys. </summary>
    public class KeyComparer<K> : IEqualityComparer<K>, IComparer<K> where K : IKey<K>
    {
        public static bool AreEqual(ref K a, ref K b)
        {
            var aGroup = a?.Group ?? default(Id<Group, K>);
            var bGroup = a?.Group ?? default(Id<Group, K>);
            if (!Id<Group, K>.AreEqual(ref aGroup, ref bGroup)) return false;
            var aName = a?.Name ?? default(Id<Name, K>);
            var bName = b?.Name ?? default(Id<Name, K>);
            if (!Id<Name, K>.AreEqual(ref aName, ref bName)) return false;
            return true;
        }

        public bool Equals(K a, K b) => AreEqual(ref a, ref b);

        public static int GetHashCode(ref K k) 
        {
            return k.Group.GetHashCode() * 37 + k.Name.GetHashCode();
        }

        public int GetHashCode(K k) => GetHashCode(ref k);

        public static int Compare(ref K a, ref K b) 
        {
            var aGroup = a?.Group ?? default(Id<Group, K>);
            var bGroup = b?.Group ?? default(Id<Group, K>);
            var compareGroup = Id<Group, K>.Compare(ref aGroup, ref bGroup);
            if (compareGroup != 0) return compareGroup;

            var aName = a?.Name ?? default(Id<Name, K>);
            var bName = b?.Name ?? default(Id<Name, K>);
            var compareName = Id<Name, K>.Compare(ref aName, ref bName);
            return compareName;
        }

        public int Compare(K a, K b) => Compare(ref a, ref b);
    }
}