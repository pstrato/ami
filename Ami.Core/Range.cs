using System;
using System.Collections.Generic;

namespace Ami.Core
{
    /// <summary> Range of comparable values. </summary>
    public struct Range<T> : IEquatable<Range<T>> where T : IComparable<T>
    {
        public Range(T lowerValue, bool lowerInclusive, T upperValue, bool upperInclusive)
        {
            LowerValue = lowerValue;
            LowerInclusive = lowerInclusive;
            UpperValue = upperValue;
            UpperInclusive = upperInclusive;
        }

        public T LowerValue { get; }

        public bool LowerInclusive { get; }

        public T UpperValue { get; }

        public bool UpperInclusive { get; }

        public override int GetHashCode()
        {
            unchecked
            {
                return LowerInclusive.GetHashCode() + LowerValue.GetHashCode() + UpperInclusive.GetHashCode() + UpperValue.GetHashCode();
            }
        }

        public override bool Equals(object obj) => obj is Range<T> r && AreEqual(ref this, ref r);

        public bool Equals(Range<T> other) => AreEqual(ref this, ref other);

        public Range<V> As<V>(Func<T, V> transform) where V: IComparable<V>
        {
            if (transform == null) throw new ArgumentNullException(nameof(transform));
            return new Range<V>(transform(LowerValue), LowerInclusive, transform(UpperValue), UpperInclusive);
        }

        public static bool AreEqual(ref Range<T> a, ref Range<T> b)
        {
            return a.LowerInclusive == b.LowerInclusive && a.UpperInclusive && b.UpperInclusive
                && Comparer<T>.Default.Compare(a.LowerValue, b.LowerValue) == 0 && Comparer<T>.Default.Compare(a.UpperValue, b.UpperValue) == 0;
        }

        /// <summary> Range from an inclusive value. </summary>
        public static Range<T> From(T value) => new Range<T>(value, true, default(T), false);

        /// <summary> Range to an inclusive value. </summary>
        public static Range<T> To(T value) => new Range<T>(default(T), false, value, true);

        /// <summary> Range after an exclusive value. </summary>
        public static Range<T> After(T value) => new Range<T>(value, false, default(T), false);

        /// <summary> Range before an exlusive value. </summary>
        public static Range<T> Before(T value) => new Range<T>(default(T), false, value, false);

        /// <summary> Range between inclusive values. </summary>
        public static Range<T> Between(T from, T to) => new Range<T>(from, true, to, true);
    }
}