namespace Ami.Core
{
    /// <summary> Marks identifiable resources. </summary>
    public interface IIdentifiable { }
}