using System;
using System.Collections.Generic;
using System.Linq;
using Ami.Core;

namespace Ami.Core
{
    /// <summary> Key with groups and identifier. </summary>
    public interface IKey<K>
    {
        /// <summary> Key group. </summary>
        Id<Group, K> Group { get; }

        /// <summary> Key name. </summary>
        Id<Name, K> Name { get; }
    }
}