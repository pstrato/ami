namespace Ami.Core
{
    /// <summary> Name of a key. </summary>
    public abstract class Name: IIdentifiable {}
}