using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Ami.Core {

    /// <summary> Helper methods for stream manipulation. </summary>
    public static class StreamHelper {

        /// <summary> Read all bytes from a stream. </summary>
        public static List<byte> ReadAllBytes(this Stream content) {
            var bytes = new List<byte>();
            var buffer = new byte[1024];
            int bytesRead;
            content.Position = 0;
            while ((bytesRead = content.Read(buffer, 0, buffer.Length)) > 0) {
                bytes.AddRange(buffer.Take(bytesRead));
            }
            return bytes;
        }
    }
}