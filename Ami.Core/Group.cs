using System;

namespace Ami.Core
{
    /// <summary> Group of a key. </summary>
    public abstract class Group: IIdentifiable { }
}