using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Ami.Core.Collections {

    [TestFixture] public class TestSlicedEnumerable {

        [Test] public void ThrowsIfArgumentNull() {
            Assert.That(() =>SlicedEnumerableHelper.All((SlicedEnumerable<int>)null), Throws.ArgumentNullException);
            Assert.That(() =>SlicedEnumerableHelper.AwaitAll<int>((Task<ISlicedEnumerable<int>>)null), Throws.ArgumentNullException);
        }

        [Test] public async Task StartsTaskBeforeYield() {
            var slicedEnumerable = new SlicedEnumerable<int>(
                new Task<IEnumerable<int>>(() => Enumerable.Range(0, 5))
            );
            var all = await slicedEnumerable.All();
            Assert.That(all, Is.EqualTo(Enumerable.Range(0, 5)));
        }

        [Test] public void NonGenericCallsReturnsEnumerator() {
            var slicedEnumerable = new SlicedEnumerable<int>(
                new Task<IEnumerable<int>>(() => Enumerable.Range(0, 5)),
                new Task<IEnumerable<int>>(() => Enumerable.Range(0, 10))
            );
            var count = 0;
            foreach (var task in (slicedEnumerable as IEnumerable)) ++count;
            Assert.That(count, Is.EqualTo(2));
        }

        [Test] public void ThrowsWithNullTask() {
            var slicedEnumerable = new SlicedEnumerable<int>(
                new Task<IEnumerable<int>>(() => Enumerable.Range(0, 5)),
                null
            );
            Assert.That(async () => await slicedEnumerable.All(), Throws.InvalidOperationException);
        }

        [Test] public void EnumeratorMoveNextThrowsIfDisposed() {
            var slicedEnumerable = new SlicedEnumerable<int>(
                new Task<IEnumerable<int>>(() => Enumerable.Range(0, 5))
            );
            var slicedEnumerator = slicedEnumerable.GetEnumerator();
            slicedEnumerator.Dispose();
            Assert.That(() => slicedEnumerator.MoveNext(), Throws.Exception.TypeOf(typeof(ObjectDisposedException)));
        }

        [Test] public void EnumeratorResetThrowsIfDisposed() {
            var slicedEnumerable = new SlicedEnumerable<int>(
                new Task<IEnumerable<int>>(() => Enumerable.Range(0, 5))
            );
            var slicedEnumerator = slicedEnumerable.GetEnumerator();
            slicedEnumerator.Dispose();
            Assert.That(() => slicedEnumerator.Reset(), Throws.Exception.TypeOf(typeof(ObjectDisposedException)));
        }

        [Test] public void EnumeratorCurrentThrowsIfDisposed() {
            var slicedEnumerable = new SlicedEnumerable<int>(
                new Task<IEnumerable<int>>(() => Enumerable.Range(0, 5))
            );
            var slicedEnumerator = slicedEnumerable.GetEnumerator();
            Assert.That(() => { 
                slicedEnumerator.MoveNext();
                slicedEnumerator.Dispose();
                slicedEnumerator.Current.ToString();
            }, Throws.Exception.TypeOf(typeof(ObjectDisposedException)));
        }

        [Test] public async Task EnumeratorResetYieldsAllTasks() {
            var slicedEnumerable = new SlicedEnumerable<int>(
                new Task<IEnumerable<int>>(() => Enumerable.Range(0, 5)),
                new Task<IEnumerable<int>>(() => Enumerable.Range(5, 5))
            );
            var slicedEnumerator = slicedEnumerable.GetEnumerator();
            while (slicedEnumerator.MoveNext()) {
                await slicedEnumerator.Current;
            }
            var all = new List<int>();
            slicedEnumerator.Reset();
            while (slicedEnumerator.MoveNext()) {
                all.AddRange(await slicedEnumerator.Current);
            }
            Assert.That(all, Is.EqualTo(Enumerable.Range(0, 10)));
        }
    }
}