using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Ami.Core.Collections {

    [TestFixture] public class TestSlicedEnumerableHelper {

        [Test] public void AllThrowsWithNullArgument() {
            Assert.That(() => SlicedEnumerableHelper.All(null as ISlicedEnumerable<int>), Throws.ArgumentNullException);
        }

        [Test] public void AllReturnsCombinedEnumerables() {
            var groupedEnumerable = new SlicedEnumerable<int>(Task.FromResult(Enumerable.Range(0, 3)), Task.FromResult(Enumerable.Range(3, 4)));
            Assert.That(groupedEnumerable.All, Is.EqualTo(Enumerable.Range(0, 7)));
        }

        [Test] public void AllReturnsEnumerablesInOrder() {
            var groupedEnumerable = new SlicedEnumerable<int>(
                Task.Run(() => { Thread.Sleep(100); return Enumerable.Range(0, 3); }), 
                Task.Run(() => { Thread.Sleep(10); return Enumerable.Range(3, 4); }) 
            );
            Assert.That(groupedEnumerable.All, Is.EqualTo(Enumerable.Range(0, 7)));
        }
    }
}