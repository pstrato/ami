using System;
using System.Threading;
using System.Threading.Tasks;

namespace Ami.Test {

    public static class WaitFor {

        public static async Task IsTrue(this Func<Task<bool>> test, int atMost = 1000) {
            var start = DateTime.Now;
            do {
                Thread.Sleep(1);
            } while (!await test() && (DateTime.Now - start).TotalMilliseconds > atMost);
        }
    }
}