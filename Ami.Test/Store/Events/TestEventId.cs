using System;
using Ami.Test;
using NUnit.Framework;

namespace Ami.Storage.Events {

    public class TestEventId {

        [Test] public void ConvertsToUtc() {
            var eventId = new EventId(DateTime.Now);
            Assert.That(eventId.Timestamp.Kind == DateTimeKind.Utc);
        }

        [Test] public void AreComparable() {
            var now = DateTime.UtcNow;
            var EventId1 = new EventId(now.AddDays(-1));
            var EventId2 = new EventId(now.AddDays(+1));

            ComparableTest.Run(EventId1, EventId2, 
                (a, b) => a < b, (a, b) => a <= b,
                (a, b) => a == b, (a, b) => a != b,
                (a, b) => a > b, (a, b) => a >= b
            );
        }

        [Test] public void CanParseToString() {
            var eventId = EventId.ForNow;
            Assert.That(EventId.Parse(eventId.ToString()), Is.EqualTo(eventId));
        }

        [Test] public void ParseThrowsIfArgumentNull() {
            Assert.That(() => EventId.Parse(null), Throws.ArgumentNullException);
        }

        [Test] public void AssertIsValidThrowsIfDefault() {
            Assert.That(() => default(EventId).AssertIsValid(), Throws.ArgumentException);
        }

        [Test] public void AssertIsValidDoesNotThrowIfNotDefault() {
            EventId.ForNow.AssertIsValid();
        }
    }
}