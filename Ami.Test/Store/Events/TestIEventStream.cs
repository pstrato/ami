using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ami.Core.Collections;
using Ami.Storage.Events.Transactions;
using Ami.Storage.Events.Memory;
using Ami.Storage.Events.Local;
using Ami.Storage.Resources;
using NUnit.Framework;

namespace Ami.Storage.Events {

    [TestFixture, Parallelizable] public class TestIEventStream {

        public static Func<IEventStream>[] All => new Func<IEventStream>[]{
            () => new MemoryEventStream(),
            //() => new LocalEventStream("")
        };

        [Test, TestCaseSource(typeof(TestIEventStream), "All")] public async Task IsEmptyIntially(Func<IEventStream> test){
            using (var eventStream = test()) {
                Assert.That(await eventStream.Events().AwaitAll(), Is.Empty);
            }
        }

        [Test, TestCaseSource(typeof(TestIEventStream), "All")] public void CommitThrowsIfTransactionIsNull(Func<IEventStream> test){
            using (var eventStream = test()) {
                Assert.That(() => eventStream.Commit(null), Throws.ArgumentNullException);
            }
        }

        [Test, TestCaseSource(typeof(TestIEventStream), "All")] public async Task CommitsAreReturned(Func<IEventStream> test) {
            using (var eventStream = test()) {
                var before = EventId.ForNow;
                
                var transaction1 = new CreateFile(new Path<File>("/test1"));
                var eventId1 = await eventStream.Commit(transaction1);
                

                var transaction2 = new CreateFile(new Path<File>("/test2"));
                var eventId2 = await eventStream.Commit(transaction2);

                var after = EventId.ForNow;
                
                {
                    var events = await eventStream.Events().AwaitAll();
                    Assert.That(events.Count(), Is.EqualTo(2));
                    Assert.That(events.ElementAt(0).Id, Is.LessThan(after).And.GreaterThan(before).And.EqualTo(eventId1));
                    Assert.That(events.ElementAt(0).Transaction, Is.EqualTo(transaction1));
                    Assert.That(events.ElementAt(1).Id, Is.LessThan(after).And.GreaterThan(before).And.EqualTo(eventId2));
                    Assert.That(events.ElementAt(1).Transaction, Is.EqualTo(transaction2));
                }

                {
                    var eventsBefore = await eventStream.Events(before:before).AwaitAll();
                    Assert.That(eventsBefore, Is.Empty);
                }
                
                {
                    var eventsAfter = await eventStream.Events(after: after).AwaitAll();
                    Assert.That(eventsAfter, Is.Empty);
                }

                {
                    var eventsAfter1 = await eventStream.Events(after: eventId1).AwaitAll();
                    Assert.That(eventsAfter1.Count(), Is.EqualTo(1));
                    Assert.That(eventsAfter1.ElementAt(0).Id, Is.EqualTo(eventId2));
                    Assert.That(eventsAfter1.ElementAt(0).Transaction, Is.EqualTo(transaction2));
                }

                {
                    var eventsBefore1 = await eventStream.Events(before: eventId1).AwaitAll();
                    Assert.That(eventsBefore1.Count(), Is.EqualTo(0));
                }

                {
                    var eventsBefore2 = await eventStream.Events(before: eventId2).AwaitAll();
                    Assert.That(eventsBefore2.Count(), Is.EqualTo(1));
                    Assert.That(eventsBefore2.ElementAt(0).Id, Is.EqualTo(eventId1));
                    Assert.That(eventsBefore2.ElementAt(0).Transaction, Is.EqualTo(transaction1));
                }
                {
                    var eventsAfter2 = await eventStream.Events(after: eventId2).AwaitAll();
                    Assert.That(eventsAfter2.Count(), Is.EqualTo(0));
                }
            }
        }

        [TestCaseSource(typeof(TestIEventStream), "All")] public async Task OnCommitActionsAreTriggered(Func<IEventStream> test) {
            using (var eventStream = test()) {
                var called = false;
                using (var connection = await eventStream.OnCommit(() => called = true)) {                    
                    await eventStream.Commit(new CreateFile(new Path<File>("/a")));
                    Thread.Sleep(10);
                    Assert.IsTrue(called);
                }
                called = false;
                await eventStream.Commit(new CreateFile(new Path<File>("/b")));
                Thread.Sleep(10);
                Assert.IsFalse(called);
            }
        }

        [TestCaseSource(typeof(TestIEventStream), "All")] public async Task TransactionLastEventIdsMustMatch(Func<IEventStream> test) {
            using (var eventStream = test()) {
                var initial = new TransactionGroup(
                    new CreateFile(new Path<File>("/file")),
                    new InsertIntoFile(new Path<File>("/file"), null, 0, 1, 2, 3)
                );
                await eventStream.Commit(initial);
                
                var beforeInitial = new TransactionGroup(
                    new ReplaceIntoFile(new Path<File>("/file"), new EventId(EventId.ForNow.Timestamp.AddSeconds(-1)), 0, 4, 5, 6)
                );
                Assert.That(async () => await eventStream.Commit(beforeInitial), Throws.InvalidOperationException);

                var afterInitial = new TransactionGroup(
                    new ReplaceIntoFile(new Path<File>("/file"), EventId.ForNow, 0, 4, 5, 6)
                );
                Assert.That(async () => await eventStream.Commit(afterInitial), Throws.InvalidOperationException);
            }
        }
    }
}