using System.Collections.Generic;
using Ami.Storage.Resources;
using NUnit.Framework;

namespace Ami.Storage.Events.Transactions {

    [TestFixture] public class TestValidationContext {

        [Test] public void ExpectsTransaction() {
            Assert.That(() => new ValidationContext(EventId.ForNow, async path => default(File)).AssertCanApply(null), Throws.ArgumentNullException);
        }
    }
}