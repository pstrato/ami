using System;
using System.Collections.Generic;
using Ami.Storage.Resources;
using NUnit.Framework;

namespace Ami.Storage.Events.Transactions {

    [TestFixture] public class TestTransactions {

        static EventId TestEventId = EventId.ForNow;

        static Path<File> TestPath = new Path<File>("/test");

        public static Func<Transaction>[] AllExpectsNonRoot => new Func<Transaction>[]{
            () => new AppendToFile(default(Path<File>), EventId.ForNow),
            () => new CreateFile(default(Path<File>)),
            () => new DeleteFile(default(Path<File>), EventId.ForNow),
            () => new InsertIntoFile(default(Path<File>), EventId.ForNow, 0, 1),
            () => new OverrideFile(default(Path<File>), EventId.ForNow, 1),
            () => new RemoveFromFile(default(Path<File>), EventId.ForNow, 0, 1),
            () => new ReplaceIntoFile(default(Path<File>), EventId.ForNow, 0, 1)
        };

        [Test, TestCaseSource("AllExpectsNonRoot")] public void ExpectsNonRoot(Func<Transaction> test) {
            Assert.That(() => test(), Throws.ArgumentException);
        }


        public static (Func<Transaction>, Action<Transaction>)[] AllKeepsData => new (Func<Transaction>, Action<Transaction>)[] {
            (() => new AppendToFile(TestPath, TestEventId, 1), t => Assert.Multiple(() => {
                Assert.That((t as AppendToFile).Path, Is.EqualTo(TestPath));
                Assert.That((t as AppendToFile).LastEventId, Is.EqualTo(TestEventId));
                Assert.That((t as AppendToFile).Data, Is.EqualTo(new byte[]{1}));      
            })),
            (() => new CreateFile(TestPath), t => Assert.That((t as CreateFile).Path, Is.EqualTo(TestPath))),
            (() => new DeleteFile(TestPath, TestEventId), t => Assert.Multiple(() => {
                Assert.That((t as DeleteFile).Path, Is.EqualTo(TestPath));
                Assert.That((t as DeleteFile).LastEventId, Is.EqualTo(TestEventId));
            })),
            (() => new InsertIntoFile(TestPath, TestEventId, 3, 1), t => Assert.Multiple(() => {
                Assert.That((t as InsertIntoFile).Path, Is.EqualTo(TestPath));
                Assert.That((t as InsertIntoFile).LastEventId, Is.EqualTo(TestEventId));
                Assert.That((t as InsertIntoFile).Offset, Is.EqualTo(3));
                Assert.That((t as InsertIntoFile).Data, Is.EqualTo(new byte[]{1}));      
            })),
            (() => new OverrideFile(TestPath, TestEventId, 1), t => Assert.Multiple(() => {
                Assert.That((t as OverrideFile).Path, Is.EqualTo(TestPath));
                Assert.That((t as OverrideFile).LastEventId, Is.EqualTo(TestEventId));
                Assert.That((t as OverrideFile).Data, Is.EqualTo(new byte[]{1}));      
            })),
            (() => new RemoveFromFile(TestPath, TestEventId, 3, 1), t => Assert.Multiple(() => {
                Assert.That((t as RemoveFromFile).Path, Is.EqualTo(TestPath));
                Assert.That((t as RemoveFromFile).LastEventId, Is.EqualTo(TestEventId));
                Assert.That((t as RemoveFromFile).Offset, Is.EqualTo(3));
                Assert.That((t as RemoveFromFile).Length, Is.EqualTo(1));      
            })),
            (() => new ReplaceIntoFile(TestPath, TestEventId, 3, 1), t => Assert.Multiple(() => {
                Assert.That((t as ReplaceIntoFile).Path, Is.EqualTo(TestPath));
                Assert.That((t as ReplaceIntoFile).LastEventId, Is.EqualTo(TestEventId));
                Assert.That((t as ReplaceIntoFile).Offset, Is.EqualTo(3));
                Assert.That((t as ReplaceIntoFile).Data, Is.EqualTo(new byte[]{1}));      
            }))
        };

        [Test, TestCaseSource("AllKeepsData")] public void KeepsData((Func<Transaction>, Action<Transaction>) test) {
            test.Item2(test.Item1());
        }

        public static Func<(Transaction, ValidationContext)>[] AllValidationStopsIfFileHasError => new Func<(Transaction, ValidationContext)>[] {
            () => {
                
                return (new TransactionGroup(
                    new CreateFile(TestPath),
                    new AppendToFile(TestPath, EventId.ForNow, 1)
                ), new ValidationContext(EventId.ForNow, async path => new File(TestPath, EventId.ForNow, 0)));
            },
            () => {
                
                return (new TransactionGroup(
                    new DeleteFile(TestPath, EventId.ForNow),
                    new CreateFile(TestPath)
                ), new ValidationContext(EventId.ForNow, async path => default(File)));
            },
            () => {
                
                return (new TransactionGroup(
                    new CreateFile(TestPath),
                    new DeleteFile(TestPath, EventId.ForNow)
                ), new ValidationContext(EventId.ForNow, async path => new File(TestPath, EventId.ForNow, 0)));
            },
            () => {
                
                return (new TransactionGroup(
                    new CreateFile(TestPath),
                    new InsertIntoFile(TestPath, EventId.ForNow, 0, 1)
                ), new ValidationContext(EventId.ForNow, async path => new File(TestPath, EventId.ForNow, 0)));
            },
            () => {
                
                return (new TransactionGroup(
                    new CreateFile(TestPath),
                    new OverrideFile(TestPath, EventId.ForNow, 1)
                ), new ValidationContext(EventId.ForNow, async path => new File(TestPath, EventId.ForNow, 0)));
            },
            () => {
                
                return (new TransactionGroup(
                    new CreateFile(TestPath),
                    new RemoveFromFile(TestPath, EventId.ForNow, 0, 1)
                ), new ValidationContext(EventId.ForNow, async path => new File(TestPath, EventId.ForNow, 0)));
            },
            () => {
                
                return (new TransactionGroup(
                    new CreateFile(TestPath),
                    new ReplaceIntoFile(TestPath, EventId.ForNow, 0, 1)
                ), new ValidationContext(EventId.ForNow, async path => new File(TestPath, EventId.ForNow, 0)));
            }
        };

        [Test, TestCaseSource("AllValidationStopsIfFileHasError")] public void ValidationStopsIfFileAlreadyHasError(Func<(Transaction, ValidationContext)> test) {
            
            var (transaction, context) = test();
            Assert.That(() => context.AssertCanApply(transaction), Throws.InvalidOperationException);
        }

        public static Func<Transaction>[] AllExpectsEventId() => new Func<Transaction>[] {
            () => new DeleteFile(TestPath, default(EventId)),
            () => new OverrideFile(TestPath, default(EventId), 1),
            () => new RemoveFromFile(TestPath, default(EventId), 0, 1),
            () => new ReplaceIntoFile(TestPath, default(EventId), 0, 1)
        };
        
        [Test, TestCaseSource("AllExpectsEventId")] public void ExpectsEventId(Func<Transaction> test) {
            Assert.That(() => test(), Throws.ArgumentException);
        }

        public static Func<Transaction>[] AllCannotProceedIfFileDoesNotExistOrDeletedOrOutdated => new Func<Transaction>[]{
            () => new AppendToFile(TestPath, EventId.ForNow, 1),
            () => new DeleteFile(TestPath, EventId.ForNow),
            () => new InsertIntoFile(TestPath, EventId.ForNow, 0, 1),
            () => new OverrideFile(TestPath, EventId.ForNow, 1),
            () => new RemoveFromFile(TestPath, EventId.ForNow, 0, 1),
            () => new ReplaceIntoFile(TestPath, EventId.ForNow, 0, 1)
        };

        [Test, TestCaseSource("AllCannotProceedIfFileDoesNotExistOrDeletedOrOutdated")] public void CannotProceedIfFileDoNotExists(Func<Transaction> test) {
            Assert.That(() => new ValidationContext(EventId.ForNow, async path => default(File)).AssertCanApply(test()), Throws.InvalidOperationException);
        }

        [Test, TestCaseSource("AllCannotProceedIfFileDoesNotExistOrDeletedOrOutdated")] public void CannotProceedIfFileDeletedBefore(Func<Transaction> test) {
            
            var eventId = EventId.ForNow;
            var group = new TransactionGroup(
                new DeleteFile(TestPath, eventId),
                test()
            );
            Assert.That(() => new ValidationContext(EventId.ForNow, async path => new File(TestPath, eventId, 0)).AssertCanApply(group), 
                Throws.InvalidOperationException);
        }

        [Test, TestCaseSource("AllCannotProceedIfFileDoesNotExistOrDeletedOrOutdated")]  public void CannotProceedWithWrongEventId(Func<Transaction> test) {
            Assert.That(() => new ValidationContext(EventId.ForNow, async path => new File(path, TestEventId, 0)).AssertCanApply(test()), Throws.InvalidOperationException);
        }

        public static Func<Transaction>[] AllThrowsIfOffsetNegative => new Func<Transaction>[] {
            () => new InsertIntoFile(TestPath, EventId.ForNow, -3, 1),
            () => new RemoveFromFile(TestPath, EventId.ForNow, -3, 1),
            () => new ReplaceIntoFile(TestPath, EventId.ForNow, -3, 1)
        };

        [Test, TestCaseSource("AllThrowsIfOffsetNegative")] public void ThrowsIfOffsetNegative(Func<Transaction> test) {
            Assert.That(() => test(), Throws.ArgumentException);
        }

        public static Func<Transaction>[] AllThrowsIfDataNul => new Func<Transaction>[] {
            () => new AppendToFile(TestPath, EventId.ForNow, null),
            () => new InsertIntoFile(TestPath, EventId.ForNow, 0, null),
            () => new OverrideFile(TestPath, EventId.ForNow, null),
            () => new ReplaceIntoFile(TestPath, EventId.ForNow, 0, null)
        };

        [Test, TestCaseSource("AllThrowsIfDataNul")] public void ThrowsIfDataNull(Func<Transaction> test) {
            Assert.That(() => test(), Throws.ArgumentNullException);
        }

        public static Func<Transaction>[] AllThrowsIfDataEmpty => new Func<Transaction>[] {
            () => new AppendToFile(TestPath, EventId.ForNow, new byte[0]),
            () => new InsertIntoFile(TestPath, EventId.ForNow, 0, new byte[0]),
            () => new OverrideFile(TestPath, EventId.ForNow, new byte[0]),
            () => new ReplaceIntoFile(TestPath, EventId.ForNow, 0, new byte[0])
        };

        [Test, TestCaseSource("AllThrowsIfDataEmpty")] public void ThrowsIfDataEmpty(Func<Transaction> test) {
            Assert.That(() => test(), Throws.ArgumentException);
        }

        public static Func<Transaction>[] AllCannotProceedWithOffsetOutOfBounds => new Func<Transaction>[] {
            () => new InsertIntoFile(TestPath, TestEventId, 1 + 1, 1),
            () => new RemoveFromFile(TestPath, TestEventId, 1 + 1, 1),
            () => new ReplaceIntoFile(TestPath, TestEventId, 1 + 1, 1)
        };
        
        [Test, TestCaseSource("AllCannotProceedWithOffsetOutOfBounds")] public void CannotProceedWithOffsetOutOfBounds(Func<Transaction> test) {
            Assert.That(() => new ValidationContext(EventId.ForNow, async path => new File(path, TestEventId, 0)).AssertCanApply(test()), Throws.InvalidOperationException);
        }

        public static Func<Transaction>[] AllCannotProceedWithOffsetLengthOutOfBounds => new Func<Transaction>[] {
            () => new RemoveFromFile(TestPath, TestEventId, 0, 2),
            () => new ReplaceIntoFile(TestPath, TestEventId, 0, 1, 2)
        };
        
        [Test, TestCaseSource("AllCannotProceedWithOffsetLengthOutOfBounds")] public void CannotProceedWithOffsetLengthOutOfBounds(Func<Transaction> test) {
            Assert.That(() => new ValidationContext(EventId.ForNow, async path => new File(path, TestEventId, 0)).AssertCanApply(test()), Throws.InvalidOperationException);
        }
    }
}