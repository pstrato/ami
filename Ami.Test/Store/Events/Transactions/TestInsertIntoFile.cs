using System.Collections.Generic;
using Ami.Storage.Resources;
using NUnit.Framework;

namespace Ami.Storage.Events.Transactions {

    [TestFixture] public class TestInsertIntoFile {

        static Path<File> TestPath = new Path<File>("/test");

        [Test] public void CanProceedToModifyExistingFiles() {
            var eventId = EventId.ForNow;
            var group = new TransactionGroup(
                new InsertIntoFile(TestPath, eventId, 0, 1)
            );
            new ValidationContext(EventId.ForNow, async path => new File(TestPath, eventId, 0)).AssertCanApply(group);
        }

        [Test] public void CanProceedToModifyCreatedFiles() {
            var group = new TransactionGroup(
                new CreateFile(TestPath),
                new InsertIntoFile(TestPath, null, 0, 1)
            );
            new ValidationContext(EventId.ForNow, async path => default(File)).AssertCanApply(group);
        }
    }
}