using System.Collections.Generic;
using Ami.Storage.Resources;
using NUnit.Framework;

namespace Ami.Storage.Events.Transactions {

    [TestFixture] public class TestCreateFile {

        static Path<File> TestPath = new Path<File>("/test");


        [Test] public void CannotProceedIfFileAlreadyExists() {
            var group = new TransactionGroup(
                new CreateFile(TestPath)
            );
            Assert.That(() => new ValidationContext(EventId.ForNow, async path => new File(TestPath, EventId.ForNow, 0)).AssertCanApply(group), 
                Throws.InvalidOperationException);
        }

        [Test] public void CannotProceedIfFileCreatedBefore() {
            var group = new TransactionGroup(
                new CreateFile(TestPath),
                new CreateFile(TestPath)
            );
            Assert.That(() => new ValidationContext(EventId.ForNow, async path => default(File)).AssertCanApply(group), 
                Throws.InvalidOperationException);
        }

        [Test] public void CanProceedToCreateNewFiles() {
            var group = new TransactionGroup(
                new CreateFile(TestPath)
            );
            new ValidationContext(EventId.ForNow, async path => default(File)).AssertCanApply(group);
        }
    }
}