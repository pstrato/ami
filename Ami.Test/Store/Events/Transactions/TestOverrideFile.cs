using System.Collections.Generic;
using Ami.Storage.Resources;
using NUnit.Framework;

namespace Ami.Storage.Events.Transactions {

    [TestFixture] public class TestOverrideFile {

        static Path<File> TestPath = new Path<File>("/test");
        
        [Test] public void CanProceedToModifyExistingFiles() {
            var eventId = EventId.ForNow;
            var group = new TransactionGroup(
                new OverrideFile(TestPath, eventId, 1)
            );
            new ValidationContext(EventId.ForNow, async path => new File(TestPath, eventId, 0)).AssertCanApply(group);
        }
    }
}