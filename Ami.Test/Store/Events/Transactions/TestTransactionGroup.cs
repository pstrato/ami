using System.Collections.Generic;
using Ami.Storage.Resources;
using NUnit.Framework;

namespace Ami.Storage.Events.Transactions {

    [TestFixture] public class TestTransactionGroup {

        [Test] public void TransactionGroupThrowsIfTransactionsIsNull() {
            Assert.That(() => new TransactionGroup((Transaction[])null), Throws.ArgumentNullException);
        }

        [Test] public void TransactionGroupThrowsIfTransactionsIsEmpty() {
            Assert.That(() => new TransactionGroup(new Transaction[0]), Throws.ArgumentException);
        }

        [Test] public void TransactionGroupKeepsData() {
            var transaction = new ReplaceIntoFile(new Path<File>("/test"), EventId.ForNow, 3, 1);
            var group = new TransactionGroup(transaction);
            Assert.That(group.Transactions.Length, Is.EqualTo(1));
            Assert.That(group.Transactions[0], Is.EqualTo(transaction));
        }
    }
}