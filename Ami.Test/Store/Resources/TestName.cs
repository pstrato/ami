using System;
using Ami.Test;
using NUnit.Framework;

namespace Ami.Storage.Resources {

    [TestFixture] public class TestName {

        [Test] public void AreComparable() {
            Run(new Name<File>(), new Name<File>("other"));
            Run(new Name<File>("res"), new Name<File>("resource"));
            
            void Run(Name<File> r1, Name<File> r2) {
                ComparableTest.Run(r1, r2, 
                    (a, b) => a < b, (a, b) => a <= b,
                    (a, b) => a == b, (a, b) => a != b,
                    (a, b) => a > b, (a, b) => a >= b
                );
            }
        }

        [Test] public void CanParseToString() {
            var name = new Name<Folder>();
            Assert.That(new Name<Folder>(name.ToString()), Is.EqualTo(name));
            name = new Name<Folder>(null);
            Assert.That(new Name<Folder>(name.ToString()), Is.EqualTo(name));
            name = new Name<Folder>("name");
            Assert.That(new Name<Folder>(name.ToString()), Is.EqualTo(name));
        }

        [Test, TestCaseSource(typeof(Name<File>), "InvalidChars")] public void MustNotContainSpecialCharacter(char forbidden) {
            Assert.That(() => new Name<File>($"name{forbidden}"), Throws.ArgumentException);
        }

        [Test] public void AssertIsNamedThrowsWhenNotNamed() {
            Assert.That(() => default(Name<File>).AssertIsNamed(), Throws.ArgumentException);
            Assert.That(() => new Name<File>("").AssertIsNamed(), Throws.ArgumentException);
        }

        [Test] public void AssertIsNamedDoesNotThrowWhenNamed() {
            new Name<File>("resource").AssertIsNamed();
        }

        [Test] public void CombineWithRootReturnsSamePath() {
            Assert.That(default(Path<Folder>) + new Name<Folder>("test"), Is.EqualTo(new Path<Folder>("/test")));
            Assert.That(default(Path<Folder>) + new Name<File>("file"), Is.EqualTo(new Path<File>("/file")));
        }

        [Test] public void CombineWithPathReturnsFullPath() {
            Assert.That(new Path<Folder>("/test") + new Name<Folder>(""), Is.EqualTo(new Path<Folder>("/test")));
            Assert.That(new Path<Folder>("/test") + new Name<Folder>("folder"), Is.EqualTo(new Path<Folder>("/test/folder")));
            Assert.That(new Path<Folder>("/test") + new Name<File>("file"), Is.EqualTo(new Path<File>("/test/file")));
        }
    }
}