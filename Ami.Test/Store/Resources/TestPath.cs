using System;
using Ami.Test;
using NUnit.Framework;

namespace Ami.Storage.Resources {

    [TestFixture] public class TestPath {
        
        [Test] public void AreComparable() {
            var path1 = new Path<Folder>();
            var path2 = new Path<Folder>("/type/");
            
            ComparableTest.Run(path1, path2, 
                (a, b) => a < b, (a, b) => a <= b,
                (a, b) => a == b, (a, b) => a != b,
                (a, b) => a > b, (a, b) => a >= b
            );
        }

        [Test] public void CanParseToString() {
            var path = new Path<Folder>();
            Assert.That(new Path<Folder>(path.ToString()), Is.EqualTo(path));
            path = new Path<Folder>(null);
            Assert.That(new Path<Folder>(path.ToString()), Is.EqualTo(path));
            path = new Path<Folder>("/type/");
            Assert.That(new Path<Folder>(path.ToString()), Is.EqualTo(path));
        }

        [Test] public void MustStartWithSegementSeparator() {
            Assert.That(() => new Path<Folder>("a/"), Throws.ArgumentException);
        }

        [Test] public void FolderPathEndsWithSeparator() {
            Assert.That(new Path<Folder>("/a").ToString(), Is.EqualTo("/a/"));
        }

        [Test] public void FilePathDoesNotEndWithSeparator() {
            Assert.That(new Path<File>("/a").ToString(), Is.EqualTo("/a"));
            Assert.That(() => new Path<File>("/a/"), Throws.ArgumentException);
        }

        [Test] public void MustNotHave2ConsecutiveSegmentSeparators() {
            Assert.That(() => new Path<Folder>("/a//a"), Throws.ArgumentException);
        }

        [Test, TestCaseSource(typeof(Path<Folder>), "InvalidChars")] public void MustNotContainSpecialCharacter(char forbidden) {
            Assert.That(() => new Path<Folder>($"/test{forbidden}/"), Throws.ArgumentException);
            Assert.That(() => new Path<Folder>($"/{forbidden}test/"), Throws.ArgumentException);
        }

        [Test] public void ThrowsGroupAlreadyExists() {
            Assert.That(() => default(Path<File>).ThrowResourceAlreadyExists(), Throws.InvalidOperationException);
        }

        [Test] public void ThrowsGroupNotFound() {
            Assert.That(() => default(Path<File>).ThrowResourceNotFound(), Throws.InvalidOperationException);
        }

        [Test] public void RootNameIsEmpty() {
            Assert.That(new Path<Folder>().Name, Is.EqualTo(new Name<Folder>()));
        }

        [Test] public void PathNameIsLastSegment() {
            Assert.That(new Path<Folder>("/resource").Name, Is.EqualTo(new Name<Folder>("resource")));
        }

        [Test] public void RootParentFolderIsRoot() {
            Assert.That(new Path<Folder>().ParentFolder, Is.EqualTo(new Path<Folder>()));
        }

        [Test] public void FolderParentIsRoot() {
            Assert.That(new Path<Folder>("/resource").ParentFolder, Is.EqualTo(new Path<Folder>()));
        }

        [Test] public void SubFolderParentFolder() {
            Assert.That(new Path<Folder>("/folder/sub").ParentFolder, Is.EqualTo(new Path<Folder>("/folder")));
        }

        [Test] public void CombineWithRootReturnsSamePath() {
            Assert.That(default(Path<Folder>) + new Path<Folder>("/test"), Is.EqualTo(new Path<Folder>("/test")));
            Assert.That(default(Path<Folder>) + new Path<File>("/test/file"), Is.EqualTo(new Path<File>("/test/file")));
        }

        [Test] public void CombineWithPathReturnsFullPath() {
            Assert.That(new Path<Folder>("/test") + new Path<Folder>("/"), Is.EqualTo(new Path<Folder>("/test")));
            Assert.That(new Path<Folder>("/test") + new Path<Folder>("/folder"), Is.EqualTo(new Path<Folder>("/test/folder")));
            Assert.That(new Path<Folder>("/test") + new Path<File>("/file"), Is.EqualTo(new Path<File>("/test/file")));
        }

        [Test] public void AssertIsNotRootThrowsWhenRoot() {
            Assert.That(() => default(Path<Folder>).AssertIsNotRoot(), Throws.ArgumentException);
            Assert.That(() => new Path<Folder>("").AssertIsNotRoot(), Throws.ArgumentException);
        }

        [Test] public void AssertIsNotRootDoesNotThrowWhenNotRoot() {
            new Path<Folder>("/resource").AssertIsNotRoot();
        }

        [Test] public void AssertIsNotDefaultThrowsWhenDefault() {
            Assert.That(() => default(Path<File>).AssertIsNotDefault(), Throws.ArgumentException);
            Assert.That(() => new Path<File>("").AssertIsNotDefault(), Throws.ArgumentException);
        }

        [Test] public void AssertIsNotDefaultDoesNotThrowWhenNotDefault() {
            new Path<File>("/resource").AssertIsNotDefault();
        }

        [Test] public void ThrowsResourceAlreadyExists() {
            Assert.That(() => default(Path<File>).ThrowResourceAlreadyExists(), Throws.InvalidOperationException);
        }

        [Test] public void ThrowsResourceNotFound() {
            Assert.That(() => default(Path<File>).ThrowResourceNotFound(), Throws.InvalidOperationException);
        }

        [Test] public void RootFolderIncludesAnything() {
            Assert.That(default(Path<Folder>).Includes(default(Path<Folder>)), Is.True);
            Assert.That(default(Path<Folder>).Includes(new Path<Folder>("/test")), Is.True);

            Assert.That(default(Path<Folder>).Includes(default(Path<File>)), Is.True);
            Assert.That(default(Path<Folder>).Includes(new Path<File>("/test")), Is.True);
        }

        [Test] public void SubFolderDoesNotIncludeParentFolder() {
            Assert.That(new Path<Folder>("/test").Includes(default(Path<Folder>)), Is.False);
            Assert.That(new Path<Folder>("/test/folder").Includes(new Path<Folder>("/test")), Is.False);
        }

        [Test] public void FolderDoesNotIncludeSiblingResource() {
            Assert.That(new Path<Folder>("/test").Includes(new Path<Folder>("/other")), Is.False);
            Assert.That(new Path<Folder>("/test").Includes(new Path<File>("/other")), Is.False);
            Assert.That(new Path<Folder>("/test/folder").Includes(new Path<Folder>("/test/other")), Is.False);
            Assert.That(new Path<Folder>("/test/folder").Includes(new Path<File>("/test/other")), Is.False);
        }

        [Test] public void RootFolderHasNoSegments() {
            Assert.That(default(Path<Folder>).Segments(), Is.Empty);
        }

        [Test] public void TopFolderHasOneSegments() {
            Assert.That(new Path<Folder>("/test").Segments(), Is.EqualTo(new []{ new Name<Folder>("test")}));
        }

        [Test] public void SubFoldersHaveMoreThanOneSegments() {
            Assert.That(new Path<Folder>("/test/sub").Segments(), Is.EqualTo(new []{ new Name<Folder>("test"), new Name<Folder>("sub")}));
        }
    }
}