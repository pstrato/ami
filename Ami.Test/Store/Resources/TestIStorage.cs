using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ami.Core;
using Ami.Core.Collections;
using Ami.Storage.Events;
using Ami.Storage.Events.Transactions;
using Ami.Storage.Events.Memory;
using Ami.Storage.Resources.Memory;
using NUnit.Framework;

namespace Ami.Storage.Resources {

    [TestFixture] public class TestIStorage {

        public static Func<IEventStream, IStorage>[] All => new Func<IEventStream, IStorage>[]{
            eventStream => new MemoryStorage(eventStream),
        };

        [Test, TestCaseSource(typeof(TestIStorage), "All")] public async Task StartsEmpty(Func<IEventStream, IStorage> test) {
            using (var eventStream = new MemoryEventStream()) {
                using (var storage = test(eventStream)) {
                    Assert.That(await storage.ListFiles().AwaitAll(), Is.Empty);
                    Assert.That(await storage.ListFolders().AwaitAll(), Is.Empty);
                    Assert.That(await storage.ListFiles(new Path<Folder>("/test")).AwaitAll(), Is.Empty);
                    Assert.That(await storage.ListFolders(new Path<Folder>("/test")).AwaitAll(), Is.Empty);
                }
            }
        }

        [Test, TestCaseSource(typeof(TestIStorage), "All")] public async Task ReturnsCommitedFileAtRoot(Func<IEventStream, IStorage> test) {
            using (var eventStream = new MemoryEventStream()) {
                using (var storage = test(eventStream)) {
                    var eventId = await eventStream.Commit(new TransactionGroup(
                        new CreateFile(new Path<File>("/test")),
                        new InsertIntoFile(new Path<File>("/test"), null, 0, 1, 2, 3, 4)
                    ));
                    await WaitAfter(storage, eventId);
                    Assert.That(await storage.ListFiles().AwaitAll(), Is.EqualTo(new []{new File(new Path<File>("/test"), eventId, 4)}));
                    Assert.That(await storage.Get(new Path<File>("/test")), Is.EqualTo(new File(new Path<File>("/test"), eventId, 4)));
                    Assert.That(await storage.Get(default(Path<Folder>)), Is.EqualTo(new Folder(default(Path<Folder>), eventId)));
                }
            }
        }

        [Test, TestCaseSource(typeof(TestIStorage), "All")] public async Task ReturnsCommitedFileInSubFolder(Func<IEventStream, IStorage> test) {
            using (var eventStream = new MemoryEventStream()) {
                using (var storage = test(eventStream)) {
                    var eventId = await eventStream.Commit(new TransactionGroup(
                        new CreateFile(new Path<File>("/test/file")),
                        new InsertIntoFile(new Path<File>("/test/file"), null, 0, 1, 2, 3, 4)
                    ));
                    await WaitAfter(storage, eventId);
                    Assert.That(await storage.ListFiles(new Path<Folder>("/test")).AwaitAll(), Is.EqualTo(new []{new File(new Path<File>("/test/file"), eventId, 4)}));
                    Assert.That(await storage.ListFolders().AwaitAll(), Is.EqualTo(new []{new Folder(new Path<Folder>("/test"), eventId)}));
                    Assert.That(await storage.Get(new Path<File>("/test/file")), Is.EqualTo(new File(new Path<File>("/test/file"), eventId, 4)));
                    Assert.That(await storage.Get(new Path<Folder>("/test")), Is.EqualTo(new Folder(new Path<Folder>("/test"), eventId)));
                }
            }
        }

        [Test, TestCaseSource(typeof(TestIStorage), "All")] public async Task DoesNotReturnsDeletedFileAtRoot(Func<IEventStream, IStorage> test) {
            using (var eventStream = new MemoryEventStream()) {
                using (var storage = test(eventStream)) {
                    var eventId = await eventStream.Commit(new TransactionGroup(
                        new CreateFile(new Path<File>("/test")),
                        new InsertIntoFile(new Path<File>("/test"), null, 0, 1, 2, 3, 4)
                    ));
                    await WaitAfter(storage, eventId);
                    eventId = await eventStream.Commit(new TransactionGroup(
                        new DeleteFile(new Path<File>("/test"), eventId)
                    ));
                    await WaitAfter(storage, eventId);
                    Assert.That(await storage.ListFiles().AwaitAll(), Is.Empty);
                    Assert.That(await storage.Get(new Path<File>("/test")), Is.EqualTo(new File(new Path<File>("/test"), EventId.Null, 0)));
                    Assert.That(await storage.Get(default(Path<Folder>)), Is.EqualTo(new Folder(default(Path<Folder>), eventId)));
                }
            }
        }

        [Test, TestCaseSource(typeof(TestIStorage), "All")] public async Task DoesNotReturnsDeletedSubFile(Func<IEventStream, IStorage> test) {
            using (var eventStream = new MemoryEventStream()) {
                using (var storage = test(eventStream)) {
                    var eventId = await eventStream.Commit(new TransactionGroup(
                        new CreateFile(new Path<File>("/test/sub/file")),
                        new InsertIntoFile(new Path<File>("/test/sub/file"), null, 0, 1, 2, 3, 4)
                    ));
                    await WaitAfter(storage, eventId);
                    eventId = await eventStream.Commit(new TransactionGroup(
                        new DeleteFile(new Path<File>("/test/sub/file"), eventId)
                    ));
                    await WaitAfter(storage, eventId);
                    Assert.That(await storage.ListFiles().AwaitAll(), Is.Empty);
                    Assert.That(await storage.Get(new Path<File>("/test/sub/file")), Is.EqualTo(new File(new Path<File>("/test/sub/file"), EventId.Null, 0)));
                    Assert.That(await storage.Get(new Path<Folder>("/test/sub")), Is.EqualTo(new Folder(new Path<Folder>("/test/sub"), EventId.Null)));
                    Assert.That(await storage.Get(new Path<Folder>("/test")), Is.EqualTo(new Folder(new Path<Folder>("/test"), EventId.Null)));
                    Assert.That(await storage.Get(default(Path<Folder>)), Is.EqualTo(new Folder(default(Path<Folder>), eventId)));
                }
            }
        }

        [Test, TestCaseSource(typeof(TestIStorage), "All")] public async Task KeepsSubFolderIfOtherFilesExist(Func<IEventStream, IStorage> test) {
            using (var eventStream = new MemoryEventStream()) {
                using (var storage = test(eventStream)) {
                    var createId = await eventStream.Commit(new TransactionGroup(
                        new CreateFile(new Path<File>("/test/sub/file")),
                        new CreateFile(new Path<File>("/test/sub/otherfile")),
                        new InsertIntoFile(new Path<File>("/test/sub/file"), null, 0, 1, 2, 3, 4)
                    ));
                    await WaitAfter(storage, createId);
                    var eventId = await eventStream.Commit(new TransactionGroup(
                        new DeleteFile(new Path<File>("/test/sub/file"), createId)
                    ));
                    await WaitAfter(storage, eventId);
                    Assert.That(await storage.ListFiles().AwaitAll(), Is.Empty);
                    Assert.That(await storage.Get(new Path<File>("/test/sub/file")), Is.EqualTo(new File(new Path<File>("/test/sub/file"), EventId.Null, 0)));
                    Assert.That(await storage.Get(new Path<Folder>("/test/sub")), Is.EqualTo(new Folder(new Path<Folder>("/test/sub"), eventId)));
                    Assert.That(await storage.Get(new Path<Folder>("/test")), Is.EqualTo(new Folder(new Path<Folder>("/test"), createId)));
                    Assert.That(await storage.Get(default(Path<Folder>)), Is.EqualTo(new Folder(default(Path<Folder>), createId)));
                }
            }
        }

        [Test, TestCaseSource(typeof(TestIStorage), "All")] public async Task ContentIsInitallyEmpty(Func<IEventStream, IStorage> test) {
            using (var eventStream = new MemoryEventStream()) {
                using (var storage = test(eventStream)) {
                    var eventId = await eventStream.Commit(new CreateFile(new Path<File>("/test")));
                    await WaitAfter(storage, eventId);
                    using (var content = await storage.Content(new Path<File>("/test"))) {
                        var bytes = content.ReadAllBytes();
                        Assert.That(bytes, Is.Empty);
                    }
                }
            }
        }

        [Test, TestCaseSource(typeof(TestIStorage), "All")] public async Task ContentIsAppended(Func<IEventStream, IStorage> test) {
            using (var eventStream = new MemoryEventStream()) {
                using (var storage = test(eventStream)) {
                    var eventId = await eventStream.Commit(new CreateFile(new Path<File>("/test")));
                    eventId = await eventStream.Commit(new AppendToFile(new Path<File>("/test"), eventId, 1, 2, 3, 4));
                    await WaitAfter(storage, eventId);
                    using (var content = await storage.Content(new Path<File>("/test"))) {
                        var bytes = content.ReadAllBytes();
                        Assert.That(bytes, Is.EqualTo(new byte []{1, 2, 3, 4}));
                    }
                    eventId = await eventStream.Commit(new AppendToFile(new Path<File>("/test"), eventId, 4, 3, 2, 1));
                    await WaitAfter(storage, eventId);
                    using (var content = await storage.Content(new Path<File>("/test"))) {
                        var bytes = content.ReadAllBytes();
                        Assert.That(bytes, Is.EqualTo(new byte []{1, 2, 3, 4, 4, 3, 2, 1}));
                    }
                }
            }
        }

        [Test, TestCaseSource(typeof(TestIStorage), "All")] public async Task ContentIsInserted(Func<IEventStream, IStorage> test) {
            using (var eventStream = new MemoryEventStream()) {
                using (var storage = test(eventStream)) {
                    var eventId = await eventStream.Commit(new CreateFile(new Path<File>("/test")));
                    eventId = await eventStream.Commit(new InsertIntoFile(new Path<File>("/test"), eventId, 0, 1, 2, 3, 4));
                    await WaitAfter(storage, eventId);
                    using (var content = await storage.Content(new Path<File>("/test"))) {
                        var bytes = content.ReadAllBytes();
                        Assert.That(bytes, Is.EqualTo(new byte []{1, 2, 3, 4}));
                    }
                    eventId = await eventStream.Commit(new InsertIntoFile(new Path<File>("/test"), eventId, 0, 4, 3, 2, 1));
                    await WaitAfter(storage, eventId);
                    using (var content = await storage.Content(new Path<File>("/test"))) {
                        var bytes = content.ReadAllBytes();
                        Assert.That(bytes, Is.EqualTo(new byte []{4, 3, 2, 1, 1, 2, 3, 4}));
                    }
                }
            }
        }

        [Test, TestCaseSource(typeof(TestIStorage), "All")] public async Task ContentIsOverriden(Func<IEventStream, IStorage> test) {
            using (var eventStream = new MemoryEventStream()) {
                using (var storage = test(eventStream)) {
                    var eventId = await eventStream.Commit(new CreateFile(new Path<File>("/test")));
                    eventId = await eventStream.Commit(new OverrideFile(new Path<File>("/test"), eventId, 4, 3, 2, 1, 1, 2, 3, 4));
                    await WaitAfter(storage, eventId);
                    using (var content = await storage.Content(new Path<File>("/test"))) {
                        var bytes = content.ReadAllBytes();
                        Assert.That(bytes, Is.EqualTo(new byte []{4, 3, 2, 1, 1, 2, 3, 4}));
                    }
                    eventId = await eventStream.Commit(new OverrideFile(new Path<File>("/test"), eventId, 4, 3, 2, 1));
                    await WaitAfter(storage, eventId);
                    using (var content = await storage.Content(new Path<File>("/test"))) {
                        var bytes = content.ReadAllBytes();
                        Assert.That(bytes, Is.EqualTo(new byte []{4, 3, 2, 1}));
                    }
                }
            }
        }

        [Test, TestCaseSource(typeof(TestIStorage), "All")] public async Task ContentIsRemoved(Func<IEventStream, IStorage> test) {
            using (var eventStream = new MemoryEventStream()) {
                using (var storage = test(eventStream)) {
                    var eventId = await eventStream.Commit(new CreateFile(new Path<File>("/test")));
                    eventId = await eventStream.Commit(new OverrideFile(new Path<File>("/test"), eventId, 4, 3, 2, 1, 1, 2, 3, 4));
                    eventId = await eventStream.Commit(new RemoveFromFile(new Path<File>("/test"), eventId, 0, 4));
                    await WaitAfter(storage, eventId);
                    using (var content = await storage.Content(new Path<File>("/test"))) {
                        var bytes = content.ReadAllBytes();
                        Assert.That(bytes, Is.EqualTo(new byte []{1, 2, 3, 4}));
                    }
                    eventId = await eventStream.Commit(new RemoveFromFile(new Path<File>("/test"), eventId, 1, 2));
                    await WaitAfter(storage, eventId);
                    using (var content = await storage.Content(new Path<File>("/test"))) {
                        var bytes = content.ReadAllBytes();
                        Assert.That(bytes, Is.EqualTo(new byte []{1, 4}));
                    }
                }
            }
        }

        [Test, TestCaseSource(typeof(TestIStorage), "All")] public async Task ContentIsReplaced(Func<IEventStream, IStorage> test) {
            using (var eventStream = new MemoryEventStream()) {
                using (var storage = test(eventStream)) {
                    var eventId = await eventStream.Commit(new CreateFile(new Path<File>("/test")));
                    eventId = await eventStream.Commit(new OverrideFile(new Path<File>("/test"), eventId, 4, 3, 2, 1, 1, 2, 3, 4));
                    eventId = await eventStream.Commit(new ReplaceIntoFile(new Path<File>("/test"), eventId, 0, 1, 2, 3, 4));
                    await WaitAfter(storage, eventId);
                    using (var content = await storage.Content(new Path<File>("/test"))) {
                        var bytes = content.ReadAllBytes();
                        Assert.That(bytes, Is.EqualTo(new byte []{1, 2, 3, 4, 1, 2, 3, 4}));
                    }
                    eventId = await eventStream.Commit(new ReplaceIntoFile(new Path<File>("/test"), eventId, 0, 4, 3, 2, 1, 1, 2, 3, 4));
                    await WaitAfter(storage, eventId);
                    using (var content = await storage.Content(new Path<File>("/test"))) {
                        var bytes = content.ReadAllBytes();
                        Assert.That(bytes, Is.EqualTo(new byte []{4, 3, 2, 1, 1, 2, 3, 4}));
                    }
                }
            }
        }

        async Task WaitAfter(IStorage storage, EventId eventId){
            var start = DateTime.Now;
            do {
                Thread.Sleep(1);
            } while ((await storage.Get(new Path<Folder>("/"))).LastEventId < eventId && (DateTime.Now - start).TotalSeconds > 1);
        }
    }
}