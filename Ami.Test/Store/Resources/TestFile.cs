using Ami.Storage.Events;
using Ami.Test;
using NUnit.Framework;

namespace Ami.Storage.Resources {

    [TestFixture] public class TestFile {

        [Test] public void ExistsIfLastEventIdExist() {
            Assert.That(default(File).Exists, Is.False);
            Assert.That(new File(new Path<File>("/test"), EventId.ForNow, 0).Exists, Is.True);
        }
    }
}