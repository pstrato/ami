using System;
using NUnit.Framework;

namespace Ami.Test {
    public static class EquatableTest {

        public static void Run<T>(T a, T b, 
            Func<T, T, bool> eq, Func<T, T, bool> neq) where T: IEquatable<T> {
            Assert.Multiple(() => {
                Assert.That(eq(a, a), Is.True);
                Assert.That(neq(a, a), Is.False);

                Assert.That(a.Equals(a as object), Is.True);
                Assert.That(a.Equals(b as object), Is.False);
                Assert.That(a.Equals(new object()), Is.False);

                Assert.That(a.Equals(a), Is.True);
                Assert.That(a.Equals(b), Is.False);

                Assert.That(a.GetHashCode(), Is.Not.EqualTo(b.GetHashCode()));
            });
        }
    }
}