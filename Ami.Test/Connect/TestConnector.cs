
using System.Threading.Tasks;
using NUnit.Framework;
using Ami.Test;
using System;
using System.Collections.Generic;
using Ami.Connect.Synchronize;

namespace Ami.Connect
{

    [TestFixture]
    public class TestConnector
    {
        public static IEnumerable<Func<IConnector>> All => new Func<IConnector>[] {
            () => new Connector(),
            () => new SynchronizedConnector(new Connector()),
        };

        [Test, TestCaseSource("All")]
        public async Task NotificationsAreSent(Func<IConnector> createConnector)
        {
            using (var connector = createConnector())
            {
                var notified = false;
                using (await connector.Connect(() => notified = true))
                {
                    await connector.Notify();
                    Assert.That(notified, Is.True);
                }
                notified = false;
                await connector.Notify();
                Assert.That(notified, Is.False);
            }
        }
    }
}