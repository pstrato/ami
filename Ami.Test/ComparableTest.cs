using System;
using NUnit.Framework;

namespace Ami.Test {

    public static class ComparableTest {

        public static void Run<T>(T a, T b, 
            Func<T, T, bool> lt, Func<T, T, bool> let, 
            Func<T, T, bool> eq, Func<T, T, bool> neq,
            Func<T, T, bool> gt, Func<T, T, bool> get) where T: IComparable<T> {
            Assert.Multiple(() => {
                Assert.That(lt(a, b), Is.True);
                Assert.That(let(a, b), Is.True);
                Assert.That(let(a, a), Is.True);

                Assert.That(eq(a, a), Is.True);
                Assert.That(neq(a, a), Is.False);
                Assert.That(eq(a, b), Is.False);
                Assert.That(neq(a, b), Is.True);

                Assert.That(gt(b, a), Is.True);
                Assert.That(get(b, a), Is.True);
                Assert.That(get(b, b), Is.True);
                
                Assert.That(eq(b, b), Is.True);
                Assert.That(neq(b, b), Is.False);
                Assert.That(eq(b, a), Is.False);
                Assert.That(neq(b, a), Is.True);

                Assert.That(a.CompareTo(b), Is.LessThan(0));
                Assert.That(a.CompareTo(a), Is.EqualTo(0));
                Assert.That(b.CompareTo(a), Is.GreaterThan(0));
                Assert.That(b.CompareTo(b), Is.EqualTo(0));

                Assert.That(a.Equals(a as object), Is.True);
                Assert.That(a.Equals(b as object), Is.False);
                Assert.That(a.Equals(new object()), Is.False);

                Assert.That(a.GetHashCode(), Is.Not.EqualTo(b.GetHashCode()));
            });
        }
    }
}