using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Ami.Connect;
using Ami.Core;
using Ami.Create;
using Ami.Dispose;
using Ami.Serialize;
using Ami.Store.Events;
using Ami.Store.Resources;

namespace Ami.Store.Serialize
{
    using File = Ami.Store.Resources.File;

    /// <summary> Serialized storage implementation. </summary>
    public class SerializedStorage : DisposableObject, IStorage
    {
        private struct Content : ISerializableObject
        {
            private byte[] content;

            public Content(byte[] content)
            {
                this.content = content;
            }

            async Task ISerializableObject.ReadFrom(IReader reader)
            {
                using (var vr = await reader.GetVersion())
                {
                    if (vr.Version != 0) throw new InvalidDataException("Unknown version");
                    content = await vr.ReadByteArray("Content");
                }
            }

            async Task ISerializableObject.WriteTo(IWriter writer)
            {
                using (var vw = await writer.SetVersion(0))
                {
                    await vw.WriteByteArray("Content", content);
                }
            }
        }

        IEventStream eventStream;

        ISerializer serializer;

        IConnector connector;

        Scope<Path<File>, Content> fileContents;

        Scope<Path<File>, File> fileStates;

        Scope<Path<Folder>, Folder> folderStates;

        EventId lastEventId;

        public SerializedStorage(IEventStream eventStream, IFactory factory)
        {
            this.eventStream = eventStream ?? throw new ArgumentNullException(nameof(eventStream));
            (serializer, connector) = factory.Create<SerializedStorage, ISerializer, IConnector>(this).Result;
            fileContents = new Scope<Path<File>, Content>("fileContents");
            fileStates = new Scope<Path<File>, File>("fileStates");
            folderStates = new Scope<Path<Folder>, Folder>("folderStates");
        }

        public async Task<EventId> Update(EventId eventId = default(EventId))
        {
            var changed = false;
            foreach (var e in await eventStream.Events(new Range<Id<Name, EventId>>(lastEventId.Name, false, eventId.Name, true)))
            {
                await Apply(await e);
                changed = true;
            }
            if (changed) connector.Notify();
            return lastEventId;
        }

        async Task Apply(Event e)
        {
            var id = e.Id;
            var context = CreateContext(ref id);
            context.Apply(e.Transaction);
            lastEventId = id;
            var changes = context.ChangedFiles;
            using (var stream = new MemoryStream())
            {
                foreach (var change in changes)
                {
                    var file = change.Value;
                    await ApplyChange(id, file, context, stream);
                }
                lastEventId = e.Id;
            }
        }

        async Task ApplyChange(EventId id, File file, TransactionApplicationContext context, Stream buffer)
        {
            var exists = await ApplyFileChange(file, context, buffer);

            var parent = file.Path.ParentFolder;

            var root = default(Path<Folder>);
            var purge = exists;
            do
            {
                if (purge)
                {
                    var empty = !(await serializer.Groups(folderStates, parent)).Any() && !(await serializer.Keys(fileStates, group)).Any();
                    if (empty)
                    {
                        await serializer.Remove(folderStates, group, key);
                        parent = parent.ParentFolder;
                    }
                    else
                    {
                        purge = false;
                    }
                }
                if (!purge)
                {
                    await ApplyFolderChange(id, group, key, buffer);
                }
                parent = parent.ParentFolder;
                parent.AsGroupKey(out group, out key);
            } while (!Path<Folder>.AreEqual(ref parent, ref root));
        }

        async Task ApplyFolderChange(EventId id, Group folderGroup, Key folderKey, Stream buffer)
        {
            buffer.SetLength(0);
            await formatter.Write(buffer, id);
            await serializer.Write(folderStates, folderGroup, folderKey, buffer);
        }

        async Task<bool> ApplyFileChange(File file, TransactionApplicationContext context, Stream buffer)
        {
            var path = file.Path;
            path.AsGroupKey(out var group, out var key);
            if (file.Exists)
            {
                var transactions = context.GetTransactions(ref path);
                using (var content = await serializer.Read(fileContents, group, key))
                {
                    buffer.SetLength(0);
                    await content.CopyToAsync(buffer);
                }
                if (transactions != null && transactions.Count > 0)
                {
                    foreach (var transaction in transactions)
                    {
                        await transaction.ApplyTo(buffer);
                    }
                }
                await serializer.Write(fileContents, group, key, buffer);

                buffer.SetLength(0);
                await formatter.Write(buffer, file);
                await serializer.Write(fileStates, group, key, buffer);
                return true;
            }
            else
            {
                await serializer.Remove(fileContents, group, key);
                await serializer.Remove(fileStates, group, key);
                return false;
            }
        }

        TransactionApplicationContext CreateContext(ref EventId eventId)
        {
            return new TransactionApplicationContext(eventId, Get);
        }

        public async Task<Resources.File> Get(Path<File> path)
        {
            path.AsGroupKey(out var group, out var key);
            using (var stream = await serializer.Read(fileStates, group, key))
            {
                return await formatter.Read<File>(stream);
            }
        }

        public Task<Folder> Get(Path<Folder> path)
        {
            return serializer.GetFolder(this, path);
        }

        public Task<ISlicedEnumerable<Resources.File>> ListFiles(Path<Folder> basePath = default(Path<Folder>))
        {
            return serializer.ListFiles(this, basePath);
        }

        public Task<ISlicedEnumerable<Folder>> ListFolders(Path<Folder> basePath = default(Path<Folder>))
        {
            return serializer.ListFolders(this, basePath);
        }

        public Task<Stream> Content(Path<Resources.File> path)
        {
            return serializer.OpenReadStream(this, path);
        }

        public Task<IDisposable> OnChange(Action action) => connector.Connect(action);

        protected override async Task DisposeManagedResources()
        {
            await base.DisposeManagedResources();
            connector.Dispose();
            connector = null;
        }

        protected override void ClearReferences()
        {
            eventStream = null;
            serializer = null;
        }
    }
}