using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ami.Dispose;
using Ami.Create;
using Ami.Store.Events;
using Ami.Store.Events.Transactions;
using Ami.Store.Resources;
using Ami.Connect;
using Ami.Serialize;
using System.IO;
using System.Linq;
using Ami.Core;

namespace Ami.Store.Serialize
{
    using File = Ami.Store.Resources.File;

    /// <summary> Serialized event stream implementation. </summary>
    public class SerializedEventStream : DisposableObject, IEventStream
    {
        ISerializer serializer;

        IConnector connector;

        Scope<EventId, Transaction> transactions;

        Scope<Path<File>, File> files;

        public SerializedEventStream(IFactory factory)
        {
            if (factory == null) throw new ArgumentNullException(nameof(factory));

            (serializer, connector) = factory.Create<SerializedEventStream, ISerializer, IConnector>(this).Result;

            transactions = new Scope<EventId, Transaction>("transactions");
            files = new Scope<Path<File>, File>("files");
        }

        public async Task<EventId> Commit(Transaction transaction)
        {
            var id = new EventId(DateTime.UtcNow);
            var context = new ValidationContext(id, FileAccessor);
            context.AssertCanApply(transaction);
            
            await serializer.Write(transactions, id, transaction);

            foreach (var change in context.ChangedFiles)
            {
                await CommitChange(change.Value);
            }
            return id;
        }

        async Task CommitChange(File file)
        {
            if (file.Exists)
            {
                await serializer.Write(files, file.Path, file);
            }
            else
            {
                await serializer.Remove(files, file.Path);
            }
        }

        async Task<File> FileAccessor(Path<File> path)
        {
            return await serializer.Read(files, path);
        }

        public async Task<IEnumerable<Task<Event>>> Events(Range<Id<Name, EventId>> range)
        {
            return (await serializer.Names(transactions, default(Id<Group, EventId>), range)).Select(key => Event(key));
        }

        async Task<Event> Event(Id<Name, EventId> name)
        {
            var id = new EventId(default(Id<Group, EventId>), name);
            var transaction = await serializer.Read(transactions, id);
            return new Event(id, transaction);
        }

        public Task<IDisposable> OnCommit(Action action) => connector.Connect(action);

        protected override async Task DisposeManagedResources()
        {
            await base.DisposeManagedResources();
            connector.Dispose();
            connector = null;
        }

        protected override void ClearReferences()
        {
            serializer = null;
        }
    }
}