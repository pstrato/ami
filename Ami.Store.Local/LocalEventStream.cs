﻿using System;
using System.Threading.Tasks;
using Ami.Core.Collections;
using Ami.Store.Events.Transactions;

namespace Ami.Store.Events.Local {

    /// <summary> Event stream stored on local disk. </summary>
    public class LocalEventStream : IEventStream {

        string storagePath;

        public LocalEventStream(string storagePath) {
            
        }

        public Task<EventId> Commit(Transaction transaction)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Task<ISlicedEnumerable<Event>> Events(EventId after = default(EventId), EventId before = default(EventId))
        {
            throw new NotImplementedException();
        }

        public Task<IDisposable> OnCommit(Action action)
        {
            throw new NotImplementedException();
        }
    }
}
