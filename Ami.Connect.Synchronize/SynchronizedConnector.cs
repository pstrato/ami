using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Ami.Synchronize;

namespace Ami.Connect.Synchronize
{

    /// <summary> Synchronized connector decorator. </summary>
    public class SynchronizedConnector : SynchronizedObject, IConnector
    {
        IConnector connector;

        bool disposeConnector;

        public SynchronizedConnector(IConnector connector, bool disposeConnector = true)
        {
            this.connector = connector ?? throw new ArgumentNullException(nameof(connector));
            this.disposeConnector = disposeConnector;
        }

        public async Task<IDisposable> Connect(Action action)
        {
            if (action == null) throw new ArgumentNullException(nameof(action));
            return await WithWriteLock(() => connector.Connect(action));
        }
        public async Task Notify() => await WithReadLock(() => connector.Notify());

        protected override async Task DisposeManagedResources()
        {
            await base.DisposeManagedResources();
            if (disposeConnector) connector.Dispose();
            connector = null;
        }
    }
}