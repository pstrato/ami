﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ami.Create.Base
{
    /// <summary> Base factory. </summary>
    public class BaseFactory : IFactory
    {
        Dictionary<Type, Dictionary<Type, Func<object, Task<object>>>> factories;

        public BaseFactory()
        {
            factories = new Dictionary<Type, Dictionary<Type, Func<object, Task<object>>>>();
        }

        /// <summary> Define how target instances of <typeparamref name="T"/> are created for source instances of <typeparamref name="S">. </summary>
        public void Set<S, T>(Func<S, Task<T>> factory)
        {
            if (factory == null) throw new ArgumentNullException(nameof(factory));
            var sourceType = typeof(S);
            if (!factories.TryGetValue(sourceType, out var sourceFactories)) factories.Add(sourceType, sourceFactories = new Dictionary<Type, Func<object, Task<object>>>());
            var targetType = typeof(T);
            Func<object, Task<object>> genericFactory = async source => await factory((S)source);
            sourceFactories[targetType] = genericFactory;
        }

        public async Task<T> Create<S, T>(S source)
        {
            var sourceType = typeof(S);
            var targetType = typeof(T);
            var selectedSourceFactory = default((Type sourceType, Type targetType, Func<object, Task<object>> factory));
            foreach (var sourceFactory in factories)
            {
                if (sourceFactory.Key.IsAssignableFrom(sourceType))
                {
                    foreach (var targetFactory in sourceFactory.Value)
                    {
                        if (targetFactory.Key.IsAssignableFrom(targetType))
                        {
                            if (selectedSourceFactory.sourceType == null)
                            {
                                selectedSourceFactory = (sourceFactory.Key, targetFactory.Key, targetFactory.Value);
                            }
                            else if (selectedSourceFactory.sourceType == sourceFactory.Key)
                            {
                                if (selectedSourceFactory.targetType.IsAssignableFrom(targetFactory.Key))
                                {
                                    selectedSourceFactory = (sourceFactory.Key, targetFactory.Key, targetFactory.Value);
                                }
                            }
                            else if (selectedSourceFactory.sourceType.IsAssignableFrom(sourceFactory.Key))
                            {
                                selectedSourceFactory = (sourceFactory.Key, targetFactory.Key, targetFactory.Value);
                            }
                        }
                    }
                }
            }
            if (selectedSourceFactory.sourceType == null) throw new ArgumentOutOfRangeException($"No compatible factory found for source {sourceType} and target {targetType} types");
            return (T)(await selectedSourceFactory.factory(source));
        }
    }
}
