﻿using System;
using System.Threading.Tasks;

namespace Ami.Connect {

    /// <summary> Maintains disposable connection. </summary>
    public interface IConnector: IDisposable {

        /// <summary> Connect an action. Connection is terminated by disposing the returned object. </summary>
        Task<IDisposable> Connect(Action action);

        /// <summary> Notify connections. </summary>
        Task Notify();
    }
}
