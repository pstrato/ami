using System;
using System.Threading;
using System.Threading.Tasks;
using Ami.Dispose;

namespace Ami.Connect
{
    /// <summary> Base connection. </summary>
    public sealed class Connection : DisposableObject
    {

        Func<Connection, Task> dispose;

        Action notify;

        public Connection(Func<Connection, Task> dispose, Action notify)
        {
            this.dispose = dispose ?? throw new ArgumentNullException(nameof(dispose));
            this.notify = notify ?? throw new ArgumentNullException(nameof(notify));
        }

        public void Notify() => notify();

        protected override async Task DisposeManagedResources()
        {
            await base.DisposeManagedResources();
            await dispose(this);
        }

        protected override void ClearReferences()
        {
            dispose = null;
            notify = null;
        }
    }
}