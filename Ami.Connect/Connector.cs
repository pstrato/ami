using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Ami.Dispose;

namespace Ami.Connect.Synchronize
{

    /// <summary> Utility class managing notification connections. </summary>
    public class Connector : DisposableObject
    {
        HashSet<Connection> connections;

        public Connector()
        {
            connections = new HashSet<Connection>();
        }

        public Task<IDisposable> Connect(Action action)
        {
            if (action == null) throw new ArgumentNullException(nameof(action));
            var connection = new Connection(DisposeConnection, action);
            connections.Add(connection);
            return Task.FromResult(connection as IDisposable);
        }

        Task DisposeConnection(Connection connection)
        {
            connections.Remove(connection);
            return Task.CompletedTask;
        }

        /// <summary> Notify all connections. </summary>
        public Task Notify()
        {
            foreach (var connection in connections)
            {
                connection.Notify();
            }
            return Task.CompletedTask;
        }

        protected override void ClearReferences()
        {
            connections = null;
        }
    }
}